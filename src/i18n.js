import i18n from 'i18next';
import XHR from 'i18next-xhr-backend';

const languageDetector = {
    type: 'languageDetector',
    async: true, // flags below detection to be async
    detect: () => { return 'en';},
    init: () => {},
    cacheUserLanguage: () => {}
};

i18n
    .use(XHR)
    .use(languageDetector)
    .init({
        compatibilityJSON: 'v3',
        whitelist: ['en', 'fr'],
        //default language
        //lng: 'en',
        fallbackLng: 'en',
        ns: ['common','errors','infos'],
        defaultNS: 'common',
        //debug: true,
        backend: {
            loadPath: '/locales/{{lng}}/{{ns}}.json',
            // allow cross domain requests
            crossDomain: true,
        }
    });

export default i18n;
