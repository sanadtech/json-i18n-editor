import { routerReducer as routing} from 'react-router-redux'
import { combineReducers } from 'redux';
import { reducer as reduxFormReducer} from 'redux-form';
import keywords from './KeywordsReducers';
import modal from './ModalReducer';
import login from './LoginReducer';
import projects from './ProjectsReducer';
import branches from './BranchesReducer';
import files from './FilesReducer';
import tree from  './TreeReducer';
import step from './stepReducer';
import mode from './modeReducer';
import load from './LoadingReducer';
import error from './ErrorReducer';
import info from './InfoReducer';
import historic from './HistoricReducer';
import columnWidths from './ColumnWidthsReducer';
import markdownEditor from './MarkdowEditorReducer';
const rootReducer = combineReducers({
    keywords,
    modal,
    login,
    projects,
    branches,
    step,
    tree,
    files,
    load,
    mode,
    error,
    info,
    historic,
    columnWidths,
    markdownEditor,
    form : reduxFormReducer,
    routing
});

export default rootReducer