/**
 * Created by IMAD on 04/05/2017.
 */

import ActionTypes from  "../constants/ActionTypes";
import _ from "lodash";

const initialState ={
    historic :[],
    fetching:false,
    fetched : false,
    error : null
}

export default (state=initialState,action)=>{

    switch (action.type) {


        case ActionTypes.FETCH_HISTORIC: {
            const {historic}=action;
            return {...state, fetched: true, historic, error:null }
        }

        case ActionTypes.ADD_HISTORIC: {
            return {...state,historic: [action.historic,...state.historic],fetched:true}
        }

        case ActionTypes.DELETE_HISTORIC: {
            let historic= state.historic.splice(action.index, 1);
            return {
                ...state,
                historic
            }
        }

        case ActionTypes.INITIAL_STATE:{
            return {...initialState}
        }

        default:{
            return state;
        }

    }
}