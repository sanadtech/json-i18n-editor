/**
 * Created by IMAD on 18/04/2017.
 */

import ActionTypes from  "../constants/ActionTypes";

const initialState ={
    branches :[],
    filteredBranches:[],
    fetching : false,
    fetched : false,
    error : null
}

export default (state=initialState,action)=>{

    switch (action.type) {

        case ActionTypes.FETCH_BRANCHES_PENDING: {
            const {fetching} =action;
            return {...state, fetching}
        }

        case ActionTypes.FETCH_BRANCHES_REJECTED: {
            const {error,fetching}=action;
            return {...state, fetched: false,  error, fetching}
        }

        case ActionTypes.FETCH_BRANCHES_FULFILLED: {
            const {branches,fetching}=action;
            return {...state,  fetched: true, branches,filteredBranches: branches,fetching, error:null }
        }

        case ActionTypes.FILTER_BRANCHES:{
            const {query}=action;
            if (query === null)  return {...state, filteredBranches: state.branches};
            const filteredBranches =state.branches.filter(branch=> {return  branch.name.toLowerCase().indexOf(query.toLowerCase()) > -1;});
            return {...state,filteredBranches}
        }

        case ActionTypes.INITIAL_STATE:{
            return {...initialState}
        }

        default:{
            return state;
        }

    }

}