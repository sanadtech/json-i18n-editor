/**
 * Created by IMAD on 09/04/2017.
 */


import ActionTypes from  "../constants/ActionTypes";

const initialState ={
    translationModal :{ open :false, data: {key : 'title' , sourceValue :'' , targetValue:''}, indexRow: ''},
    navigationModal:{ open : false , newPage :'/'},
    deleteModal : {open:false , key: '' },
    commitModal : {open : false},
    diffModal : {open : false},
    addKeyModal : {open : false}
};
export default (state=initialState,action)=>{

    switch (action.type) {
        case ActionTypes.OPEN_COMMIT_MODAL: {
            const {open } = action ;
            return {...state, commitModal: {open}}
        }

        case ActionTypes.CLOSE_COMMIT_MODAL: {
            const {open } = action ;
            return {...state, commitModal: {open}}
        }
        case ActionTypes.OPEN_DIFF_MODAL: {
            const {open } = action ;
            return {...state, diffModal: {open}}
        }

        case ActionTypes.CLOSE_DIFF_MODAL: {
            const {open} = action;
            return {...state, diffModal: {open}}
        }

        case ActionTypes.OPEN_ADD_KEY_MODAL: {
            const {open } = action ;
            return {...state, addKeyModal: {open}}
        }

        case ActionTypes.CLOSE_ADD_KEY_MODAL: {
            const {open } = action ;
            return {...state, addKeyModal: {open}}
        }

        case ActionTypes.OPEN_TRANSLATION_MODAL:{
            const {open , data , indexRow} = action ;
            return  {...state, translationModal:{ open , data , indexRow}};
        }

        case ActionTypes.CLOSE_TRANSLATION_MODAL:{
            const {open} = action ;
            return  {...state, translationModal:{ open , data : '' , indexRow :''}};
        }

        case ActionTypes.OPEN_DELETE_MODAL:{
            const {open , key} = action ;
            return  {...state, deleteModal:{ open , key}};
        }

        case ActionTypes.CLOSE_DELETE_MODAL:{
            const {open} = action ;
            return  {...state, deleteModal:{ open , key :''}};
        }

        case ActionTypes.OPEN_NAVIGATION_MODAL:{
            const {open , newPage} = action ;
            return  {...state, navigationModal:{ open , newPage}};
        }

        case ActionTypes.CLOSE_NAVIGATION_MODAL:{
            const {open} = action ;
            return  {...state, navigationModal:{ open , newPage :'/'}};
        }

        case ActionTypes.INITIAL_STATE:{
            return {...initialState}
        }

        default:{
            /**
             * default case means that the `action.type`
             * did not match any `type` supported by this
             * `reducer` so not need to perform an action
             * just return the `state` as it is
             */
            return state;
        }

    }
}