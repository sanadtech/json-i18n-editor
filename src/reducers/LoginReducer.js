/**
 * Created by IMAD on 18/04/2017.
 */


import ActionTypes from  "../constants/ActionTypes";


let initialState ={
    isLogged : false,
    fetching : false,
    privateToken : null ,
    username:null,
    error : null
};


export default (state=initialState,action)=>{

    switch (action.type) {

        case ActionTypes.LOGIN_START: {
            return {...state, fetching: action.fetching }
        }

        case ActionTypes.SET_TOKEN_USERNAME:{
            const {privateToken,username,isLogged}=action;
            return {...state, privateToken,username,isLogged}
        }

        case ActionTypes.LOGIN_FULLFILLED: {
            const {privateToken,username}=action;
            return {...state,  isLogged:true , privateToken , username}
        }

        case ActionTypes.LOGIN_REJECT: {
            const {fetching,error}=action;
            return {...state, fetching , isLogged:false , error}
        }

        case ActionTypes.INITIAL_STATE:{
            return {...initialState}
        }

        default:{
            return state;
        }

    }

}