/**
 * Created by IMAD on 18/04/2017.
 */

import ActionTypes from  "../constants/ActionTypes";

const initialState ={
    key: 250,
    sourceValue: 400,
    targetValue: 400,
    options: 150
}



export default (state=initialState,action)=>{

    if (action.type === ActionTypes.RESIZE_TABLE) {
        const {columnKey,newColumnWidth}=action;
        return  {...state, [columnKey] : newColumnWidth} ;

    }
    return  state
}