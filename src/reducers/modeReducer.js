/**
 * Created by IMAD on 06/04/2017.
 */

import ActionTypes from  "../constants/ActionTypes";

const initialState ={
    mode : 'write',
    open : false,
    anchorEl : {}
}

export default (state=initialState,action)=>{

    switch (action.type) {
        case ActionTypes.CHANGE_MODE: {
            return  {...state, mode : action.mode};
        }

        case ActionTypes.OPEN_FORM: {
            return  {...state, open : action.open};
        }

        case ActionTypes.OPEN_DROPDOWN: {
            let {open,anchorEl} = action;
            return  {...state, open , anchorEl};
        }

        case ActionTypes.CLOSE_FORM: {
            return  {...state, open : action.open};
        }

        case ActionTypes.INITIAL_STATE:{
            return {...initialState}
        }

        default:{
            return state;
        }
    }
}