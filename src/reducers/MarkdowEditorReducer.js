/**
 * Created by IMAD on 17/05/2017.
 */


import ActionTypes from  "../constants/ActionTypes";


let initialState ={
    targetValue: '',
    sourceValue: '',
    selection: null
};


export default (state=initialState,action)=>{

    switch (action.type) {

        case ActionTypes.EDIT_MARKDOWN_TARGET_VALUE: {
            return {...state, targetValue: action.targetValue }
        }

        case ActionTypes.EDIT_MARKDOWN_SOURCE_VALUE: {
            return {...state, sourceValue: action.sourceValue }
        }

        case ActionTypes.OPEN_TRANSLATION_MODAL : {
            return {...state, targetValue : action.data.targetValue , sourceValue: action.data.sourceValue }
        }

        case ActionTypes.INITIAL_STATE:{
            return {...initialState}
        }

        default:{
            return state;
        }

    }

}