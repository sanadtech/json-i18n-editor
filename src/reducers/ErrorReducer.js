/**
 * Created by IMAD on 26/04/2017.
 */


import ActionTypes from "../constants/ActionTypes";
import I18n from "../i18n";

const initialState ={
    open: false,
    error : null
};

export default (state=initialState,action)=>{

    if(action.error !=='' && typeof action.error !== 'undefined'){

        // use error.status for more accurate error messages
        // use localised error messages please
        let endUserErrorMessage = '';
        switch(action.error.status){
            case 100:
                endUserErrorMessage = action.key + ' ' +I18n.t('errors:err100');
                break;
            case 111:
                endUserErrorMessage = I18n.t('errors:err111');
                break;
            case 112:
                endUserErrorMessage = I18n.t('errors:err112');
                break;
            case 400:
                endUserErrorMessage = I18n.t('errors:err400');
                break;
            case 401:
                endUserErrorMessage = I18n.t('errors:err401');
                break;
            case 403:
                endUserErrorMessage = I18n.t('errors:err403');
                break;
            case 404:
                endUserErrorMessage = I18n.t('errors:err404');
                break;
            case 405:
                endUserErrorMessage = I18n.t('errors:err405');
                break;
            case 409:
                endUserErrorMessage = I18n.t('errors:err405');
                break;
            case 422:
                endUserErrorMessage = I18n.t('errors:err405');
                break;
            case 430:
                endUserErrorMessage = I18n.t('errors:err430');
                break;
            case 500:
                endUserErrorMessage = I18n.t('errors:err500');
                break;
            default:
                endUserErrorMessage = I18n.t('errors:errDefault');
                break;
        }
        return  {...state, open : true ,error:endUserErrorMessage};
    }
    if (action.type===ActionTypes.CLOSE_ERROR) return {...state, error:null , open : action.payload};
    else if (action.type === ActionTypes.INITIAL_STATE) return {...initialState};
    else return state;

}