/**
 * Created by IMAD on 18/04/2017.
 */

import ActionTypes from  "../constants/ActionTypes";

const initialState ={
    projects :[],
    filteredProjects:[],
    fetching: false,
    fetched : false,
    error : null
}

export default (state=initialState,action)=>{

    switch (action.type) {

        case ActionTypes.FETCH_PROJECTS_PENDIND: {
            const {fetching} =action;
            return {...state, fetching}
        }

        case ActionTypes.FETCH_PROJECTS_REJECTED: {
            const {error,fetching}=action;
            return {...state, fetched: false,  error, fetching}
        }

        case ActionTypes.FETCH_PROJECTS_FULFILLED: {
            const {projects,fetching}=action;
            return {...state,  fetched: true, projects,filteredProjects: projects,fetching, error:null }
        }

        case ActionTypes.FILTER_PROJECTS:{
            const {query}=action;
            if (query === null)  return {...state, filteredProjects: state.projects};
            const filteredProjects =state.projects.filter(project=> {return  project.name.toLowerCase().indexOf(query.toLowerCase()) > -1;});
            return {...state, filteredProjects}
        }

        case ActionTypes.INITIAL_STATE:{
            return {...initialState}
        }

        default:{
            return state;
        }

    }

}