/**
 * Created by IMAD on 06/04/2017.
 */

import ActionTypes from  "../constants/ActionTypes";
import _ from "lodash";

const initialState ={
    initialKeywords:[],
    keywords :[],
    filtredKeyword:[],
    nbrUntransKeys : 0,
    nbrEmptyKeys : 0,
    filter : '',
    eltFiltered : '',
    fetching:false,
    fetched : false,
    modified: false,
    committed:false,
    changes : [],
    error : null,
    added:null
};

export default (state=initialState,action)=>{

    switch (action.type) {

        case ActionTypes.FETCH_KEYWORDS_PENDING: {
            const {fetching} =action;
            return {...state, fetching}
        }

        case ActionTypes.FETCH_KEYWORDS_REJECTED: {
            const {error,fetching}=action;
            return {...state, fetched: false,  error, fetching}
        }

        case ActionTypes.FETCH_KEYWORDS_SOURCE_FULFILLED: {
            const {keywords,fetching,initialKeywords}=action;
            return {...state, fetched: true, keywords,fetching, filtredKeyword:keywords,initialKeywords,changes:[],error:{status:null,message:null} }
        }

        case ActionTypes.FETCH_KEYWORDS_TARGET_FULFILLED: {
            let sourceKeyWords = [...state.keywords];
            let {
                initialKeywords: targetInitialJson,
                keywords: targetKeyWords,
                fetching
            } = action;

            _.each(sourceKeyWords, (source) =>{
                let target = _.find(targetKeyWords, (target)=> { return source.key === target.key;});
                // use the target value if the key in teh source list is already translated
                source.targetValue = target ? target.targetValue : '';
            });

            _.each(targetKeyWords, (target) =>{
                let source = _.find(sourceKeyWords, (source)=> { return source.key === target.key;});
                // add the missing item from the target list if it's missing in the source list
                if(!source ) {
                    sourceKeyWords.push({
                        ...target,
                        sourceValue: '',
                    })
                }
            });

            return {
                ...state,
                fetched: true,
                keywords: sourceKeyWords,
                initialKeywords: targetInitialJson,
                filtredKeyword: sourceKeyWords,
                changes:[],
                fetching,
                error: null
            }
        }

        case ActionTypes.FIND_KEYWORD:{
            const isExist = [...state.keywords].find(word => word.key === action.key);
            if (typeof isExist !== 'undefined')return {...state,error:{status:'err100',message:"duplicated key"},added:''};
            return {...state,error:{status:null,message:null}}
        }

        case ActionTypes.ADD_KEYWORD: {
            let {key,targetValue}=action;
            let newKey = {key,sourceValue:targetValue ,targetValue};
            return {...state,keywords: [...state.keywords,newKey],filtredKeyword: [newKey,...state.keywords],changes:[{value:newKey,added:true},...state.changes],modified:true,error:{status:null,message:null},added:'info001'}
        }

        case ActionTypes.UPDATE_KEYWORD: {
            const {key,targetValue} = action;
            // update keywords
            let keywords = [...state.keywords];
            const wordToUpdate = keywords.findIndex(word => word.key === key);
            keywords[wordToUpdate].targetValue = targetValue;
            // update filtredKeyword
            let filtredKeyword = [...state.filtredKeyword];
            const wordToUpdateFiltred = filtredKeyword.findIndex(word => word.key === key);
            filtredKeyword[wordToUpdateFiltred].targetValue = targetValue;
            // update changes
            let changes = [...state.changes];
            const changeToUpdate = changes.findIndex(change => change.value.key === key);
            (changeToUpdate!==-1)?
                changes[changeToUpdate] = {value: keywords[wordToUpdate],modified:true} :
                changes.push({value: keywords[wordToUpdate],modified:true})
            return {...state, keywords, filtredKeyword,changes, modified: true}
        }

        case ActionTypes.DELETE_KEYWORD: {
            let {key}= action;
            let removedElt = [...state.keywords].find(elt => elt.key === key);
            let keywords =[...state.keywords].filter(elt => elt.key !== key);
            let filtredKeyword=[...state.filtredKeyword].filter(elt => elt.key !== key);
            return {...state, keywords, filtredKeyword, modified: true, changes:[{value: removedElt,removed:true},...state.changes]}
        }

        case ActionTypes.FILTER_KEYWORDS:{
            const {elt,query}=action;

            if (query === null)  return {...state, filtredKeyword: state.keywords ,  filter : ''};

            const filteredKeywords =state.keywords.filter((obj)=> {return  obj[elt].toLowerCase().indexOf(query.toLowerCase()) > -1;});

            return {
                ...state,
                filtredKeyword: filteredKeywords,
                eltFiltered : elt,
                filter : query
            }

        }

        case ActionTypes.UNTRANSLATABLE_KEYWORDS:{

            if(action.checked){
                const filteredKeywords =state.keywords.filter(elt => elt.targetValue === '');
                return {...state, filtredKeyword: filteredKeywords}
            }else{
                return {...state, filtredKeyword: state.keywords }
            }

        }

        case ActionTypes.NBR_UNTRANSLATABLE_KEYWORDS:{
            const nbrUntransKeys =[...state.keywords ].filter(elt => elt.targetValue === '').length;
            return {...state, nbrUntransKeys }
        }

        case ActionTypes.NBR_EMPTY_KEYWORDS:{
            const nbrEmptyKeys =[...state.keywords].filter(elt => elt.sourceValue === '').length;
            return {...state, nbrEmptyKeys }
        }

        case ActionTypes.COMMIT_KEYWORDS:{
            const {fetching,committed,modified} = action;
            return {...state, fetching ,committed,modified}
        }

        case ActionTypes.INITIAL_STATE:{
            return initialState
        }

        default:{
            return state;
        }

    }
}
