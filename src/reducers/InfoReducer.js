/**
 * Created by IMAD on 26/04/2017.
 */


import ActionTypes from  "../constants/ActionTypes";
import I18n from '../i18n';

const initialState ={
    open: false,
    info : null
};

export default (state=initialState,action)=>{

    if(action.info !=='' && typeof action.info !== 'undefined'){
        let endUserInfoMessage = '';
        switch(action.info){
            case 1:
                endUserInfoMessage = I18n.t('infos:info001');
                break;
            case 2:
                endUserInfoMessage = I18n.t('infos:info002');
                break;
            case 3:
                endUserInfoMessage = I18n.t('infos:info003');
                break;
            default:
                endUserInfoMessage = I18n.t('infos:infoDefault');
                break;
        }
        return  {...state, open : true ,info:endUserInfoMessage};
    }
    if (action.type===ActionTypes.CLOSE_INFO) return {...state, info:null , open : action.payload};
    else if( action.type===ActionTypes.INITIAL_STATE) return {...initialState};
    return state;

}