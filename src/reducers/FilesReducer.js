/**
 * Created by IMAD on 19/04/2017.
 */

import ActionTypes from  "../constants/ActionTypes";

const initialState ={
    host : '',
    projectId:'',
    projectName:'',
    branch:'',
    sourceFile:{id:'',path:'',name:''},
    targetFile:{id:'',path:'',name:''},
    dateUpdate : '',
}

export default (state=initialState,action)=>{

    switch (action.type) {

        case ActionTypes.ADD_HOST:{
            const {host}=action;
            return {...state, host}
        }

        case ActionTypes.ADD_PROJECT:{
            const {projectId,projectName}=action;
            return {...state, projectId,projectName}
        }

        case ActionTypes.ADD_BRANCH:{
            return {...state, branch:action.payload}
        }

        case ActionTypes.ADD_SOURCE_FILE:{
            return {...state, sourceFile :action.payload}
        }

        case ActionTypes.ADD_TARGET_FILE:{
            return {...state, targetFile:action.payload , dateUpdate: new Date()}
        }

        case ActionTypes.ADD_FILES:{
            let  files = action.files;
            files.dateUpdate = new Date();
            return {...files}
        }


        default:{
            return state;
        }

    }

}