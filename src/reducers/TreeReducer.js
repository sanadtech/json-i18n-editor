import ActionTypes from  "../constants/ActionTypes";


const insertChild=(oldList,list,pathname)=>{
    const index = oldList.findIndex(child => child.path === pathname);
    if(index!==-1) { oldList[index].children = {list,pathname};return oldList;}
    else {
        oldList.forEach(item=>{if (item.type==="tree") {item.children.list = insertChild(item.children.list, list, pathname);}});
        return oldList;
    }
};

const initialState ={
    list:[],
    pathname:'',
    fetching :false,
    fetched : false,
    error : null
};

export default (state=initialState,action)=>{
    //  state = Immutable.fromJS(state); // converting state to an immutable object
    switch (action.type) {

        case ActionTypes.FETCH_TREE_PENDING: {
            const {fetching} =action;
            return {...state, fetching}
        }

        case ActionTypes.FETCH_TREE_REJECTED: {
            const {error,fetching}=action;
            return {...state, fetched: false,  error, fetching}
        }

        case ActionTypes.FETCH_TREE_FULFILLED: {
            const {list,pathname,fetching} = action;
            list.forEach((item)=> {item.children={list:[],pathname:''}});
            return {...state, fetching, list,pathname, error:null, fetched: true }
        }

        case ActionTypes.FETCH_TREE_CHILDREN_FULFILLED: {
            const {list,pathname} = action;
            list.forEach((item)=> {
                if(item.type==="tree")item.children={list:[],pathname:''}
            });
            const newList =insertChild(state.list,list,pathname);
            return {...state, fetching: false, fetched: true, list:newList,pathname, error:null }
        }

        case ActionTypes.INITIAL_STATE:{
            return initialState
        }
        default:{
            return state;
        }
    }
}