/**
 * Created by IMAD on 06/04/2017.
 */

import ActionTypes from  "../constants/ActionTypes";

const initialState ={
    stepIndex :0 ,
    isFinished : false
}

export default (state=initialState,action)=>{

    switch (action.type) {
        case ActionTypes.NEXT_STEP: {
            return  {...state, stepIndex : 1 , isFinished : true};
        }
        case ActionTypes.PREV_STEP: {
            return {...state, stepIndex: 0 , isFinished: false}
        }

        case ActionTypes.SET_STEP:{
            const {stepIndex} = action;
            return {...state, stepIndex}
        }

        case ActionTypes.FINISH_STEP:{
            return {...state, stepIndex: 0 , isFinished: false}
        }
        default:{
            /**
             * it is necessary to return the `state`
             * when no `action.type` is supported by this
             * `reducer`
             */
            return state;
        }
    }
}