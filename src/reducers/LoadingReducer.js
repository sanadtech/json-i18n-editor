/**
 * Created by IMAD on 26/04/2017.
 */



const initialState ={
    queue :[],
  // use a queue instead
}

export default (state=initialState,action)=>{

    if(action.fetching ) return {...state, queue :[...state.queue,action.type]};

    else if (action.fetching === false) {
        state.queue.pop();
        return {...state};
    }

    else return  state;

}