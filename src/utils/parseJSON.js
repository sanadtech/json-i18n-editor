/**
 * Created by IMAD on 04/04/2017.
 */

/**
 * this file should not be amongst the ActionTypes
 * cause does'nt contain `ActionTypes`, what you should
 * have done is to put it in a folder named e.g. `utils`
 * and so similar files
 */

import _ from 'lodash';
/*
export function recursiveObjectSource(node, parent_string) {
    let GlobalResult = [];
    _.forIn(node,(value,key)=>  {
        let result=[];
        let ret ={};
        if (Array.isArray(value)) {
            if (parent_string === '') {
                _.extend(result, recursiveObjectSource(value,'(' + key + ')'));
            }
            else {
                _.extend(result, recursiveObjectSource(value, parent_string +'.(' + key + ')'));
            }
        }
        else if (typeof(value) === "object") {
            if (parent_string === '') {
                _.extend(result, recursiveObjectSource(value, key));
            }
            else {
                _.extend(result, recursiveObjectSource(value, parent_string + '.' + key));
            }
        }
        else {
            if (parent_string === '') {
                ret.key =key;
            }
            else {
                ret.key =parent_string + '.' + key;
            }
            ret.sourceValue= value;
            ret.targetValue= value;
            result.push(ret);
        }
        GlobalResult= _.concat(GlobalResult, result);
    });
    return GlobalResult;
}

 function convertToObject (key,targetValue) {
     let ret = {};
     if (key.lastIndexOf('.') !== -1) {
         let value = {};
         value[key.slice(key.lastIndexOf('.') + 1, key.length)] = targetValue;
         key = key.slice(0, key.lastIndexOf('.'));
         return convertToObject(key, value);
     } else {
         ret[key] = targetValue;
         return ret;
     }
 }

 function convertToArray(json){
     _.forIn(json,(value,key)=>  {
         if(key.indexOf('(')===0) {
             let list = [];
             let newKey = key.slice(1, key.length - 1);
             Object.keys(value).map(e => list.push(value[e]));
             _.unset(json, key);
             _.set(json, newKey, list);
         }
         if(typeof (value) === "object"){
            convertToArray(value)
         }
     });

export function generateJSON(keywords){

     //convert to json
     let json = {};
     keywords.forEach((item)=> {
          let obj=convertToObject(item.key,item.targetValue);
         _.merge(json,obj);
    });

    //convert object list to array
    convertToArray(json);
    return json;
}
 }
*/
export function recursiveObjectSource(node, parent_string) {
    let GlobalResult = [];
    _.forIn(node,(value,key)=>  {
        let result=[];
        let ret ={};

        if (typeof(value) === "object") {
            if (Array.isArray(value)) {
                let objectValue={};
                _.forIn(value,(value,key)=>  {
                    let newKey='['+key+']';
                    objectValue[newKey]=value
                });
                value=objectValue;
            }

            if (parent_string === '') {
                _.extend(result, recursiveObjectSource(value, key));
            }
            else {
                _.extend(result, recursiveObjectSource(value, parent_string + '.' + key));
            }
        }
        else {
            if (parent_string === '') {
                ret.key =key;
            }
            else {
                ret.key =parent_string + '.' + key;
            }
            ret.sourceValue= value;
            ret.targetValue= value;
            result.push(ret);
        }
        GlobalResult= _.concat(GlobalResult, result);
    });
    return GlobalResult;
}

export function recursiveObjectTarget(node, parent_string) {
    let GlobalResult = [];
    _.forIn(node,(value,key)=>  {
        let result=[];
        let ret ={};

        if (typeof(value) === "object") {
            if (Array.isArray(value)) {
                let objectValue={};
                _.forIn(value,(value,key)=>  {
                    let newKey='['+key+']';
                    objectValue[newKey]=value
                });
                value=objectValue;
            }
            if (parent_string === '') {
                _.extend(result, recursiveObjectTarget(value, key));
            }
            else {
                _.extend(result, recursiveObjectTarget(value, parent_string + '.' + key));
            }
        }
        else {
            if (parent_string === '') {
                ret.key =key;
            }
            else {
                ret.key =parent_string + '.' + key;
            }
            ret.targetValue= value;
            result.push(ret);
        }
        GlobalResult= _.concat(GlobalResult, result);
    });
    return GlobalResult;
}



export function generateJSON(keywords){
    let json = {};
    keywords.forEach((item)=> {
        _.set(json,item.key,item.targetValue);
    });
    return json;
}