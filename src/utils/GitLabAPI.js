/**
 * Created by IMAD on 12/04/2017.
 */

/*
 Commandline for quick reference:
 user=<your_user>
 password=<your_password>
 clear #you put your password in your bash historic, but at least you removed it from your terminal
 baseurl=https://gitlab.com
 curl $baseurl/api/v3/session --data-urlencode "login=$user" --data-urlencode "password=$password"
 private_token=<your_privte_token> #get this from the json response
 curl $baseurl/api/v3/projects?private_token=$token
 project_id=<your_project_id> #pick one from your projects (you need to owner to see this)
 filepath=<your_test_file_path>
 curl "$baseurl/api/v3/projects/$project_id/repository/files?file_path=$filepath&ref=master&private_token=$token"
 curl --request POST $baseurl/api/v3/projects/$project_id/fork --data-urlencode private_token=$token
 # Create file
 curl --request POST --data-urlencode "private_token=$token" --data-urlencode "content=Test file\n upload content" --data-urlencode "commit_message=Create file via api test" "$baseurl/api/v3//projects/$project_id/repository/files?file_path=$filepath&branch_name=master&author_email=$email"
 #Update file
 curl --request PUT --data-urlencode "private_token=x972Tr5kBqkwiZCTh_kT" --data-urlencode "content=Test file\n upload content" --data-urlencode "commit_message=Create file via api test" "https://gitlab.com/api/v3/projects/3088432/repository/files?file_path=locales/en/doc.json&branch_name=master&author_email=imane.chkikar@gmail.com"
 const ApplicationId = '1fdad68ef3ff0884055ab79df26c6325e6283e1e87989ed2579fc9046f68fd9a';
 const secretId= '4b4d874df0067306612d9899dca61c9a95149ae3f600d75caef3cc3b6376725e';
 const redirect_uri= 'http://localhost:3000/';s
 */

import axios from 'axios';

const Base64 = require('js-base64').Base64;
const projectsURl='projects';
import { v1 as uuid } from 'uuid';


const callGitLab= (path, method, args = {}, data={},callback, errorcallback)=>{
    const gitApiUrl = 'https://'+window.localStorage.getItem('host')+'/api/v4';
    axios({
        method: method,
        baseURL: gitApiUrl,
        url: path,
        params:args,
        data:data,
        responseType: 'json',
    }).then((response) => {
        callback(response.data);
    }).catch((error) => {
        errorcallback(error);
    });
};

const getApiProjectPath = (projectId) => {
    return  projectsURl+'/' + projectId + '/';
};

const getApiFilesPath = (projectId,filePath) => {
    return getApiProjectPath(projectId) + 'repository/files/'+ encodeURIComponent(filePath);
};

const getApiBranchesPath=(projectId)=>{
    return getApiProjectPath(projectId)+'repository/branches'
};

const getApiTreePath=(projectId)=>{
    return getApiProjectPath(projectId)+'repository/tree'
};


export function getFileContents(projectId, filePath, token, branch='master',callback,callbackError){

    let path =   getApiFilesPath(projectId, filePath);
    let params = {'ref': branch, 'private_token': token};
    callGitLab(
            path,
            'GET',
            params,
            {},
            (result)=>{
                const base64Content = Base64.decode(result.content);
                const encodedURI = encodeURIComponent(base64Content);
                const decodedURI = decodeURIComponent(encodedURI);
                const  file = JSON.parse(decodedURI);
                callback(file);
            },
            (error)=>callbackError(error)
        );
}

export function commitFile(method,token,projectId,file_path,branch,content,commit_message,callback,callbackError){

    let path = getApiFilesPath(projectId,file_path);
    let params ={
        'private_token': token,
        'branch': branch,

        'commit_message': commit_message,
    };

    let data={ 'content': content}

    callGitLab(
          path,
         method,
         params,
        data,
        (result)=>callback(result),
        (error)=>callbackError(error)
     );
}

export function getProfile(token,username,callback,callbackError){

    let params={
        'private_token': token,
        'username' : username
    }

    callGitLab(
        'users',
        'GET',
        params,
        {},
        (result)=>callback(result),
        (error)=>callbackError(error)
    );
}

export function authentificate(username,pwd,callback,callbackError){

    let params={
        'email' : username,
        'password' : pwd
    }

    callGitLab(
        'session',
        'POST',
        params,
        {},
        (result)=>callback(result),
        (error)=>callbackError(error)
    );
    /*axios({
        method: 'POST',
        baseURL: gitUrl,
        url: 'oauth/token',
        params:
            {
                'grant_type'    : 'password',
                'username'      :  username,
                'password'      :  pwd
            }
        ,
        responseType: 'json',
    }).then((response) => {
        callback(response);
    }).catch((error) => {
        callbackError(error);
    });*/
}

export function GetProjects(token,callback,callbackError) {

    let path =   projectsURl;
    let params = {
        'membership': 'yes',
        'private_token': token,
        'per_page': 100,
        'order_by': 'last_activity_at',
        'sort': 'desc'
    };
    callGitLab(
        path,
        'GET',
        params,
        {},
        (result)=>callback(result),
        (error)=>callbackError(error)
    );

}

export function GetBranches(token,projectId,callback,callbackError){

    let path =   getApiBranchesPath(projectId);
    let params = {
        'private_token': token,
        'per_page': 100
    };
    callGitLab(
        path,
        'GET',
        params,
        {},
        (result)=>callback(result),
        (error)=>callbackError(error)
    );
}

export function GetTree(token,projectId,branch,pathTree,callback,callbackError){
    let path =   getApiTreePath(projectId);
    let params = {
        'private_token': token,
        'ref':branch
    };

    if(pathTree!=='') params.path=pathTree;

    callGitLab(
        path,
        'GET',
        params,
        {},
        (result) => {
            const files = result ? result.map((file) => ({...file, uid: uuid()})) : result;
            callback(files)
        },
        (error)=>callbackError(error)
    );
}
