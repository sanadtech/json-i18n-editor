import React from 'react';
import configureStore from './store/configureStore';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { render } from 'react-dom'
import { browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import Root from './components/Root/Root';
import { I18nextProvider } from 'react-i18next'; // as we build ourself via webpack
import i18n from './i18n'; // initialized i18next instance
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import styles from './css/layout.css';


const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

injectTapEventPlugin();
const muiTheme = getMuiTheme(styles.theme);



render(
    <I18nextProvider i18n={ i18n }>
        <MuiThemeProvider  muiTheme={muiTheme} >
            <Root store={store} history={history} />
        </MuiThemeProvider>
    </I18nextProvider>
   ,
    document.getElementById('root')
)



