/**
 * Created by IMAD on 31/03/2017.
 */

import keyMirror from 'keymirror';


/**
 * the name of the exported `object` and the
 * `filename` should be identical e.g. here
 * `ActionTypes` object should be `ActionsListe`
 * 
 * and also please try to use `english` names
 * e.g. `ActionTypes` instead of `ActionsListe`
 */
const ActionTypes = keyMirror({

    //LOGIN REDUCER
    LOGIN_START:null,
    SET_TOKEN_USERNAME:null,
    LOGIN_FULLFILLED:null,
    LOGIN_REJECT:null,
    LOGOUT:null,

    //HISTORIC REDUCER
    ADD_HISTORIC:null,
    FETCH_HISTORIC:null,
    DELETE_HISTORIC:null,

    //PROJECTS REDUCER
    FETCH_PROJECTS_PENDIND:null,
    FETCH_PROJECTS_REJECTED:null,
    FETCH_PROJECTS_FULFILLED:null,
    FILTER_PROJECTS:null,

    //BRANCHES REDUCER
    FETCH_BRANCHES_PENDING:null,
    FETCH_BRANCHES_REJECTED:null,
    FETCH_BRANCHES_FULFILLED:null,
    FILTER_BRANCHES:null,

    //MARKDOWN REDUCER
    EDIT_MARKDOWN_SOURCE_VALUE : null,
    EDIT_MARKDOWN_TARGET_VALUE : null,

    //TREE REDUCER
    FETCH_TREE_PENDING : null,
    FETCH_TREE_REJECTED:null,
    FETCH_TREE_FULFILLED:null,
    FETCH_TREE_CHILDREN_FULFILLED:null,


    //KEYWORDS REDUCERS
    FETCH_KEYWORDS_PENDING : null,
    FETCH_KEYWORDS_REJECTED:null,
    UNTRANSLATABLE_KEYWORDS:null,
    NBR_UNTRANSLATABLE_KEYWORDS:null,
    NBR_EMPTY_KEYWORDS:null,
    FETCH_KEYWORDS_SOURCE_FULFILLED:null,
    FETCH_KEYWORDS_TARGET_FULFILLED:null,
    FILTER_KEYWORDS:null,
    DELETE_KEYWORD:null,
    UPDATE_KEYWORD:null,
    UPDATE_BY_INDEX:null,
    ADD_KEYWORD:null,
    FIND_KEYWORD:null,
    COMMIT_KEYWORDS:null,


    //ERROR REDUCER
    ADD_ERROR:null,
    CLOSE_ERROR:null,

    //INFO REDUCER
    ADD_INFO:null,
    CLOSE_INFO:null,

    //FILES REDUCER
    ADD_HOST: null,
    ADD_PROJECT:null,
    ADD_BRANCH:null,
    ADD_SOURCE_FILE:null,
    ADD_TARGET_FILE:null,
    ADD_FILES:null,

    //STEP REDUCER
    NEXT_STEP:null,
    PREV_STEP:null,
    SET_STEP:null,
    FINISH_STEP:null,

    //MODAL REDUCER
    OPEN_COMMIT_MODAL:null,
    CLOSE_COMMIT_MODAL:null,
    OPEN_DIFF_MODAL:null,
    CLOSE_DIFF_MODAL:null,
    OPEN_ADD_KEY_MODAL:null,
    CLOSE_ADD_KEY_MODAL:null,
    OPEN_TRANSLATION_MODAL:null,
    CLOSE_TRANSLATION_MODAL:null,
    OPEN_DELETE_MODAL:null,
    CLOSE_DELETE_MODAL:null,
    OPEN_NAVIGATION_MODAL:null,
    CLOSE_NAVIGATION_MODAL:null,

    //TABLE REDUCER
    RESIZE_TABLE:null,

    //MODE REDUCER
    CHANGE_MODE:null,
    OPEN_FORM:null,
    OPEN_DROPDOWN : null ,
    CLOSE_FORM:null,

    //GLOBAL
    INITIAL_STATE:null,

});


export default ActionTypes;