import Layout from './components/Root/Layout'
import MainContainer from './components/MainPage/MainContainer';
import BranchesListContainer from './components/BranchesPage/BranchesListContainer'
import ImportFilesStepperContainer    from  './components/ImportFilesStepper/ImportFilesStepperContainer';
import TranslatePageContainer  from './components/TranslatePage/TranslatePageContainer';
import  ProjectListContainer  from './components/MainPage/ProjectsPage/ProjectListContainer';
import React from 'react'
import {Route,IndexRoute} from 'react-router'
import {logout}from './actions/loginActions'


export default function configRoutes(store) {

    const privateToken= store.getState().login.privateToken;

    const requireProjectId =(nextState,replace) => {
        const projectId= store.getState().files.projectId;
        if(projectId==="" || privateToken === '') {
            replace('/');
        }
    };

    const requireBranch =(nextState,replace) => {
        const branch= store.getState().files.branch;
        if(branch===""  || privateToken === '') {
            replace('/');
        }
    };

    const requireFiles =(nextState,replace) => {
        const file= store.getState().files.sourceFile.path;
        if(file==="" || privateToken === '') {
            replace('/');
        }
    };

    const requireAuth =(nextState,replace) => {
        // add an expiration check please

        if(privateToken===null ) {
            replace('/');
        }

        if(new Date(window.localStorage.getItem('expirateAt'))<= new Date())
        {
            logout();
        }
    };


    return (
        <Route path="/" component={Layout}>
            <IndexRoute component={MainContainer} />
            <Route path="/projects" component={ProjectListContainer} onEnter={requireAuth}/>
            <Route path="/projects/branches" component={BranchesListContainer} onEnter={requireProjectId} />
            <Route path="/projects/branches/files" component={ImportFilesStepperContainer} onEnter={requireBranch}/>
            <Route path="/projects/branches/files/translate" component={TranslatePageContainer} onEnter={requireFiles}/>
        </Route>
    )
}

