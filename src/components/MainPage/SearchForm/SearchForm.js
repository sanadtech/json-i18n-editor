/**
 * Created by IMAD on 05/06/2017.
 */

import React,{PropTypes} from "react";
import TextField from 'material-ui/TextField';
import styles from '../../../css/layout.css';
import { translate } from 'react-i18next';
import { Field, reduxForm } from 'redux-form';
import ActionSearch from 'material-ui/svg-icons/action/search';
import {connect} from "react-redux";



const renderTextField = ({input, label , type ,disabled, meta: { touched, error }, ...custom }) => (
    <TextField     hintText={label}
                   floatingLabelText={label}
                   errorText={touched && error}
                   disabled={disabled}
                   type={type}
                   {...input}
                   {...custom}
    />
);


const SearchForm = props => {
    const { handleSubmit,fetching,t } = props;
    let disabled= (fetching !==0) ;
    return (
        <div  style={styles.row} >
            <ActionSearch  style={{marginTop : '38px'}}/>
            <Field name="query" disabled={disabled} onChange={(event, newValue, previousValue) => handleSubmit(newValue)} component={renderTextField} label={t("Search")}  type="input" />
        </div>
    )
};

SearchForm.propTypes = {
    handleSubmit:  PropTypes.func.isRequired,
    fetching : PropTypes.number.isRequired,
};


export default connect(
    state => ({
        initialValues: {query : ""}// pull initial values from  reducer
    }),
    null            // bind  loading action
)(   reduxForm({
    form: 'SearchForm',  // a unique identifier for this form
})(translate(['common'], { wait: true })(SearchForm)));
