/**
 * Created by IMAD on 29/03/2017.
 */

import React,{PropTypes} from "react";
import LoginFormContainer   from './LoginForm/LoginFormContainer'
import ProjectListContainer from './ProjectsPage/ProjectListContainer'

class Main extends React.Component{

    componentDidMount(){
        // fire an action creator to get privateToken from locale storage instead of doing that in the reducer
        // reducers should be kept pure (cf functional programming)

        // add an expiration check please

        const {logout,setTokenUsername}=this.props;

        if(new Date(window.localStorage.getItem('expirateAt'))<= new Date()) logout();
        else if( window.localStorage.getItem('privateToken')!==null) setTokenUsername();

    }

    render(){
        const {isLogged} =this.props;
        return(
            isLogged ?  <ProjectListContainer/> : <LoginFormContainer/>
        );
    }

}

Main.propTypes = {
    setTokenUsername:  PropTypes.func.isRequired,
    logout : PropTypes.func.isRequired,
    isLogged: PropTypes.bool.isRequired
}

export default  Main;