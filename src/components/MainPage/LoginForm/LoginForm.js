/**
 * Created by IMAD on 18/04/2017.
 */
import React,{PropTypes} from "react";
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import styles from '../../../css/layout.css';
import { translate } from 'react-i18next';
import { Field, reduxForm } from 'redux-form';
import IconButton from 'material-ui/IconButton';
import ActionHelp from 'material-ui/svg-icons/action/help';
import HelpModalContainer from './HelpModal/HelpModalContainer';
import {connect} from "react-redux";


//do not use local state, keep one source of truth please => Redux

const validate = values => {
    const errors = {};
    const requiredFields = [ 'username', 'privateToken', 'host' ];
    requiredFields.forEach(field => {
        if (!values[ field ] ) {
            errors[ field ] = 'Required'
        }
        if(  values.host && !/^[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$/i.test(values.host)){
            errors[ 'host' ] = 'Invalid domain';
        }
    })
    return errors
};

const renderTextField = ({input, label , type ,meta: { touched, error }, ...custom }) => (
    <TextField     hintText={label}
                   floatingLabelText={label}
                   errorText={touched && error}
                   fullWidth={true}
                   type={type}
                   {...input}
                   {...custom}
    />
);


const LoginForm = props => {
    const { handleSubmit,fetching,showHelp,submitting, invalid ,t } = props;
    let disabled= (fetching !==0) ;
    return (
        <div  style={styles.formLogin} >
            <form  onSubmit={handleSubmit}>
                <div  style={styles.row} >
                    <Field name="username" style={styles.field} component={renderTextField} label={t("Username")} type="input"/>
                </div>
                <div style={styles.row} >
                    <Field name="privateToken"  style={styles.field}   component={renderTextField} label={t("PrivateToken")}  type="password" />
                    <IconButton tooltip={t("Help")} onTouchTap={()=>showHelp()} style={{marginTop : '24px'}} >
                        <ActionHelp />
                    </IconButton>
                </div>
                <div  style={styles.row} >
                    <Field name="host" style={styles.field} component={renderTextField} label={t("Host")} type="input"/>
                </div>
                <div style={styles.row}>
                    <RaisedButton
                        label={t("Login")} primary={false} style={styles.buttons}
                        disabled={ invalid || submitting || disabled }
                        type="submit"
                    />
                </div>
            </form>
            <HelpModalContainer/>
        </div>
    )
};

LoginForm.propTypes = {
    handleSubmit:  PropTypes.func.isRequired,
    showHelp: PropTypes.func.isRequired,
    fetching : PropTypes.number.isRequired,
};


export default connect(
    state => ({
        initialValues: {host : "gitlab.runmyprocess.com"}// pull initial values from account reducer
    }),
    null            // bind account loading action creator
)( reduxForm({
    form: 'LoginForm',  // a unique identifier for this form
    validate
})(translate(['common'], { wait: true })(LoginForm)));
