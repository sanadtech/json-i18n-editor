/**
 * Created by IMAD on 18/04/2017.
 */

import {connect} from "react-redux";
import {login} from "../../../actions/loginActions";
import {openCommitModal} from '../../../actions/modalActions';
import  LoginForm    from "./LoginForm"
import React from "react";


function LoginFormContainer ({onLogin,showHelp,fetching}){
    return(
            <LoginForm handleSubmit={onLogin} fetching={fetching} showHelp={showHelp}/>
    );
}

const mapDispatchToProps = {
    onLogin:login,
    showHelp:openCommitModal
}

const  mapStateToProp = (state)=>{
    return {
        fetching : state.load.queue.length,
    }
};

export default connect(mapStateToProp,mapDispatchToProps)(LoginFormContainer);
