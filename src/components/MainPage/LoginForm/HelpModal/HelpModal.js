/**
 * Created by IMAD on 09/04/2017.
 */

import React,{PropTypes} from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import styles from '../../../../css/layout.css';
import imgHelp from './help.png';
import { translate } from 'react-i18next';



function  HelpModal ({open,closeModal,t}) {

    let actions=[
        <FlatButton label={t("OK")} primary={false} onTouchTap={()=>closeModal()}/>
    ];
    return (
        <div>
            <Dialog
                title={t("Help")}
                modal={true}
                open={open}
                actions={actions}
                autoScrollBodyContent={true}
            >
                <div style={styles.fullWidth}>
                    <h4>What is a private token ?</h4>
                    <p style={{margin: 15, padding: 15}}>
                        Private token is a string of characters that allows you to interact with Gitlab servers.
                        Your private token is used to access the API and Atom feeds without username/password
                        authentication.
                        So, Keep your token secret, because anyone with access to them can interact with GitLab as if
                        they were you.
                    </p>
                    <h4>How to get a private token?</h4>
                    <div style={{margin: 15, padding: 15}}>
                        <ol>
                            <li> you should access to your Gitlab account.</li>
                            <li> Go to your account page <strong> /profile/account </strong> and copy your private
                                token.
                            </li>
                        </ol>
                        <strong>for more information <a href="https://docs.gitlab.com/ce/api/#private-tokens"
                                                        target="_blank"> click here</a></strong>
                        <br/>
                        <img src={imgHelp} alt="help" width="100%" height={300}/>
                    </div>
                </div>
            </Dialog>
        </div>
    );
}

HelpModal.PropTypes = {
    open:PropTypes.bool.isRequired,
    closeModal:PropTypes.func.isRequired,
};

export default translate(['common'], { wait: true })(HelpModal)
