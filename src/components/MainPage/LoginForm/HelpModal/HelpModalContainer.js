/**
 * Created by IMAD on 19/04/2017.
 */



import {connect} from "react-redux";
import {closeCommitModal} from '../../../../actions/modalActions';
import React from "react";
import  HelpModal from './HelpModal';

function TranslateModalContainer ({open,closeModal}){
    return <HelpModal open={open}   closeModal={closeModal}  />
}


const mapDispatchToProps = {
    closeModal : closeCommitModal,
};

const  mapStateToProp = (state)=>{
    return {
        open : state.modal.commitModal.open
    }
};

export default connect(mapStateToProp,mapDispatchToProps)(TranslateModalContainer);