/**
 * Created by IMAD on 18/04/2017.
 */

import {connect} from "react-redux";
import React from "react";
import {setTokenUsername,logout} from '../../actions/loginActions'
import Main from './Main';


function  MainContainer ({setTokenUsername,isLogged,logout}){
    return <Main setTokenUsername={setTokenUsername} isLogged={isLogged} logout={logout}/>;
}



const  mapStateToProp = (state)=>{
    return {isLogged : state.login.isLogged}
};

const mapDispatchToProps = {
    setTokenUsername : setTokenUsername,
    logout : logout
}

export default connect(mapStateToProp,mapDispatchToProps)(MainContainer);
