/**
 * Created by IMAD on 18/04/2017.
 */
import React, {PropTypes} from "react";
import {translate} from "react-i18next";
import {List, ListItem} from "material-ui/List";
import RaisedButton from "material-ui/RaisedButton";
import Popover from "material-ui/Popover";
import ActionGrade from "material-ui/svg-icons/action/grade";
import {blue200} from "material-ui/styles/colors";
import styles from "../../../../css/layout.css";
import ActionDelete from "material-ui/svg-icons/action/delete-forever";
import IconButton from "material-ui/IconButton";


class Historic extends React.Component {

    componentWillMount() {
        this.props.onFetchHistoric();
        this.props.onClose();
    }

    render(){
        const {t,open,anchorEl,onClose,onOpen,historic,onSelectFiles,onDeleteHistoric}=this.props;
        let i = 0;
        return(
            <div style={styles.container} >
                <RaisedButton
                    onTouchTap={(e)=>{e.preventDefault();onOpen(e.currentTarget)}}
                    label={t("PreviousFile")}
                />
                <Popover
                    open={open}
                    anchorEl={anchorEl}
                    anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                    targetOrigin={{horizontal: 'left', vertical: 'top'}}
                    onRequestClose={onClose}
                    style={styles.formLogin}
                >
                    <List style={styles.list}>
                        {
                            (historic.length===0)?
                                <div style={styles.header}>
                                <h5>{t("fileNotFound")}</h5>
                                </div>:
                                historic.map(
                                    files=>
                                        <ListItem
                                            key={i++}
                                            leftIcon={<ActionGrade color={blue200}/>}
                                            primaryText={files.projectName}
                                            secondaryText={
                                                <span>
                                                    {t("Branch") + files.branch}<br/>
                                                    {t("sourceFile") + files.sourceFile.path}<br/>
                                                    {t("targetFile") + files.targetFile.path}<br/>
                                                    {t("lastUpdate") + new Date(files.dateUpdate).toUTCString()}
                                                    </span>}
                                            onTouchTap={() => onSelectFiles(files)}
                                            rightIconButton={
                                                <IconButton tooltip={t("delete")}
                                                            onTouchTap={() => onDeleteHistoric(files)}>
                                                    <ActionDelete />
                                                </IconButton>
                                            }
                                        />

                                )
                        }
                    </List>
                </Popover>
            </div>
        );
    }
}

Historic.propTypes = {
    open: PropTypes.bool.isRequired,
    anchorEl : PropTypes.shape(),
    historic:PropTypes.array,
    onFetchHistoric:PropTypes.func.isRequired,
    onDeleteHistoric:PropTypes.func.isRequired,
    onSelectFiles:PropTypes.func.isRequired,
    onClose:PropTypes.func.isRequired,
    onOpen:PropTypes.func.isRequired,
    t:PropTypes.func.isRequired,
};

export default  translate(['common'], { wait: true })(Historic);