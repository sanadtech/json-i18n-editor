/**
 * Created by IMAD on 18/04/2017.
 */

import {connect} from "react-redux";
import {addFiles}  from '../../../../actions/FilesActions';
import {setHistoric,deleteHistoric} from '../../../../actions/HistoricActions';
import {openDropDown,closeForm} from '../../../../actions/modeActions';
import React from "react";
import Historic from "./Historic";


function HistoricContainer ({open,onOpen,onClose,historic,onFetchHistoric,onSelectFiles,onDeleteHistoric,anchorEl}){
    return(
           <Historic open={open} onFetchHistoric={onFetchHistoric} onDeleteHistoric={onDeleteHistoric} anchorEl={anchorEl}
                     onSelectFiles={onSelectFiles} onClose={onClose} onOpen={onOpen}  historic={historic}/>
    )
}

const mapDispatchToProps = {
    onFetchHistoric : setHistoric,
    onDeleteHistoric : deleteHistoric,
    onSelectFiles: addFiles,
    onClose: closeForm,
    onOpen: openDropDown
}

const  mapStateToProp = (state)=>{
    return {
        open : state.mode.open,
        anchorEl: state.mode.anchorEl,
        historic : state.historic.historic
    }
}

export default connect(mapStateToProp,mapDispatchToProps)(HistoricContainer);