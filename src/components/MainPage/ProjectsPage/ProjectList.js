/**
 * Created by IMAD on 18/04/2017.
 */
import React, {PropTypes} from "react";
import {translate} from "react-i18next";
import {List, ListItem} from "material-ui/List";
import ActionInfo from "material-ui/svg-icons/action/info";
import ActionRefresh from "material-ui/svg-icons/navigation/refresh";
import Subheader from "material-ui/Subheader";
import Avatar from "material-ui/Avatar";
import FileFolder from "material-ui/svg-icons/file/folder";
import styles from "../../../css/layout.css";
import HistoricContainer from "./Historic/HistoricContainer";
import SearchForm from '../SearchForm/SearchForm'
import IconButton from 'material-ui/IconButton';

class ProjectList extends React.Component {

    componentWillMount() {
        const {onFetchProjects,projectLength}=this.props;
        if(projectLength===0)onFetchProjects() ;
    }

    render(){
        const {t, projects, onSelectProject, onFetchProjects, onFilterProject, projectLength,fetching} = this.props;
        let i = 0 ;
        return(
            <div style={styles.container} >
                <Subheader style={styles.header} inset={true} >
                    <h4 style={{marginTop:50}}> {t("Projects")}</h4>
                    <SearchForm handleSubmit={onFilterProject} fetching={fetching}/>
                    <div style={{marginTop:30}}>
                        <HistoricContainer/>
                    </div>
                </Subheader>
                <List style={styles.list}>
                    {
                        (fetching === 0 && projectLength === 0) ?
                            <div style={styles.container}>
                                <IconButton iconStyle={styles.largeIcon}
                                            style={styles.large}
                                            onTouchTap={onFetchProjects}
                                            tooltip={t("Refresh")}
                                            tooltipPosition="top-center">
                                    <ActionRefresh/>
                                </IconButton>
                            </div>
                            : projects.map(
                            project =>
                                <ListItem
                                style={(i%2===1)? styles.oddLine : styles.pairLine}
                                key={i++}
                                leftAvatar={<Avatar icon={<FileFolder />}/>}
                                rightIcon={<ActionInfo />}
                                primaryText={project.name}
                                onTouchTap={() => onSelectProject(project.id, project.name)}
                            />

                         )}

                 </List>
            </div>
        );
    }
}

ProjectList.propTypes = {
    projects: PropTypes.arrayOf(PropTypes.shape({
        id:PropTypes.number.isRequired,
        name:PropTypes.string.isRequired,
    }).isRequired).isRequired,
    fetching: PropTypes.number.isRequired,
    projectLength: PropTypes.number.isRequired,
    onFetchProjects: PropTypes.func.isRequired,
    onSelectProject:PropTypes.func.isRequired,
    onFilterProject:PropTypes.func.isRequired,
    t:PropTypes.func.isRequired,
};

export default  translate(['common'], { wait: true })(ProjectList);