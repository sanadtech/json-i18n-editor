/**
 * Created by IMAD on 18/04/2017.
 */

import {connect} from "react-redux";
import {addProject, fetchProjects,filterProjects} from "../../../actions/projectsActions";
import React from "react";
import ProjectsList from "./ProjectList";


function ProjectListContainer({onFetchProjects, projects, onSelectProject, onFilterProject, fetching,projectLength}) {
    return(
            <ProjectsList
                projects={projects}
                fetching={fetching}
                onFetchProjects={onFetchProjects}
                onSelectProject={onSelectProject}
                onFilterProject={onFilterProject}
                projectLength={projectLength}
            />
    )
}

const mapDispatchToProps = {
    onFetchProjects : fetchProjects,
    onSelectProject : addProject,
    onFilterProject : filterProjects
}

const  mapStateToProp = (state)=>{
    return {
        projects: state.projects.filteredProjects,
        projectLength : state.projects.projects.length,
        fetching: state.load.queue.length,
    }
}

export default connect(mapStateToProp,mapDispatchToProps)(ProjectListContainer);