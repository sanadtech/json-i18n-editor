/**
 * Created by IMAD on 06/04/2017.
 */

import React from 'react';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/action/language';
import { translate } from 'react-i18next';
import i18n from '../../../i18n';
import {white} from 'material-ui/styles/colors';


function LngMenu  ({t}){
    const toggle = lng => i18n.changeLanguage(lng);
    return(
            <IconMenu
                color={white}
                iconButtonElement={<IconButton><MoreVertIcon color={white} /></IconButton>}
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                onItemTouchTap={(e, child) => {
                    toggle(child.props.value);
                }}
            >
                <MenuItem primaryText={t("lngEn")} value={t("lngEn")} />
                <MenuItem primaryText={t("lngFr")} value={t("lngFr")}/>
            </IconMenu>
    )
}


export default translate(['common'], { wait: true })( LngMenu);