/**
 * Created by IMAD on 12/04/2017.
 */

import {connect} from "react-redux";
import Header from "./Header";
import React from "react";

// it's better to use functional/dump component when we don't use locale state or do not hook react lifecycle (onComponentUpdate, ....etc)
function  HeaderContainer ({fetching,isLogged}){

    return(
            <Header  fetching={fetching} isLogged={isLogged}  />
    );
}



const  mapStateToProp = (state)=>{
    return {
        fetching : state.load.queue.length,
        isLogged: state.login.isLogged
    }
};


export default connect(mapStateToProp,null)(HeaderContainer);
