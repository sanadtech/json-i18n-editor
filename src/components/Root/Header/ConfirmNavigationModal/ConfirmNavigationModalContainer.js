/**
 * Created by IMAD on 19/04/2017.
 */



import {connect} from "react-redux";
import {closeNavigationModal} from '../../../../actions/modalActions';
import {RedirectToNewPage} from '../../../../actions/RedirectActions';
import React from "react";
import  ConfirmNavigationModal from './ConfirmNavigationModal';

function ConfirmNavigationModalContainer ({newPage,open, onRedirect,closeModal}){
    return <ConfirmNavigationModal open={open} onRedirect={onRedirect}  closeModal={closeModal} newPage={newPage}  />
}


const mapDispatchToProps = {
    closeModal : closeNavigationModal,
    onRedirect : RedirectToNewPage,
};

const  mapStateToProp = (state)=>{
    return {
        open : state.modal.navigationModal.open,
        newPage : state.modal.navigationModal.newPage
    }
};

export default connect(mapStateToProp,mapDispatchToProps)(ConfirmNavigationModalContainer);