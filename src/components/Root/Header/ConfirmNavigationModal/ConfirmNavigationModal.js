/**
 * Created by IMAD on 09/04/2017.
 */

import React,{PropTypes} from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import styles from '../../../../css/layout.css';
import { translate } from 'react-i18next';



function  ConfirmNavigationModal ({open,closeModal,onRedirect,t}) {
    const actions = [
        <FlatButton label={t("OK")} primary={true}  style={styles.buttons} autoFocus={true} onTouchTap={()=>{onRedirect();closeModal()}}/>,
        <FlatButton label={t("Cancel")} primary={false} onTouchTap={()=>closeModal()} />
    ];
    return (
        <div>
            <Dialog
                title={t("titleNavigationModal")}
                modal={true}
                open={open}
                actions={actions}
            >

                <h4>{t("BodyNavigationModal")}</h4>

            </Dialog>
        </div>
    );
}

ConfirmNavigationModal.PropTypes = {
    open:PropTypes.bool.isRequired,
    newPage:PropTypes.number.isRequired,
    closeModal:PropTypes.func.isRequired,    onRedirect: PropTypes.func.isRequired,

}

export default  translate(['common'], { wait: true })(ConfirmNavigationModal);
