/**
 * Created by IMAD on 12/04/2017.
 */

import {connect} from "react-redux";
import LoggedMenu from "./LoggedMenu";
import React from "react";
import {ChangeRoute} from '../../../../actions/RedirectActions';


function  LoggedMenuContainer( {onChangeRoute,pathname} ){
    return(
           <LoggedMenu onChangeRoute={onChangeRoute} pathname={pathname} />
    );
}


const  mapStateToProp = (state)=>{
    return {
        pathname: state.routing.locationBeforeTransitions && state.routing.locationBeforeTransitions.pathname
    }
};
const mapDispatchToProps = {
    onChangeRoute : ChangeRoute
}

export default connect(mapStateToProp,mapDispatchToProps)(LoggedMenuContainer);
