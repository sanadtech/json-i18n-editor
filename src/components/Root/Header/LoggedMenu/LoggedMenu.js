/**
 * Created by IMAD on 19/04/2017.
 */

import React, {Component,PropTypes} from 'react';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import { translate } from 'react-i18next';
import {white} from 'material-ui/styles/colors';


class LoggedMenu  extends Component{

   render(){
        const {t,pathname,onChangeRoute} = this.props;
        return(
            <IconMenu
                iconButtonElement={
                    <IconButton><MoreVertIcon  color={white}/></IconButton>
                }
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                onItemTouchTap={(e,child) =>onChangeRoute(child.props.value)}
            >
                {(pathname!=="/") &&  <MenuItem primaryText={t("Projects")} value="projects" />}
                {(pathname!=="/projects/branches") && (pathname!=="/")&&   <MenuItem primaryText={t("Branches")} value="branches" />}
                {(pathname==="/projects/branches/files/translate")  &&   <MenuItem primaryText={t("Files")} value="files" />}
                <MenuItem primaryText={t("SignOut")} value="signout"/>
            </IconMenu>
        );
    }
}

LoggedMenu.muiName = 'IconMenu';

LoggedMenu.propTypes = {
    onChangeRoute: PropTypes.func.isRequired,
    pathname: PropTypes.string.isRequired,
}


export default translate(['common'], { wait: true })( LoggedMenu);