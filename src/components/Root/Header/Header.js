/**
 * Created by IMAD on 30/03/2017.
 */

import React, {PropTypes} from 'react';
import AppBar from 'material-ui/AppBar';
import LngMenu from './LngMenu';
import { translate } from 'react-i18next';
import styles from '../../../css/layout.css';
import LinearProgress from 'material-ui/LinearProgress';
import LoggedMenuContainer from './LoggedMenu/LoggedMenuContainer'
import ConfirmNavigationModalContainer from './ConfirmNavigationModal/ConfirmNavigationModalContainer'


function Header ({t,fetching,isLogged}){
    return (
              <div>
                  <div style={styles.progress}>
                  {fetching !==0?    <LinearProgress mode="indeterminate" /> : ''}
                  </div>
                  <AppBar
                        title={t('appName')}
                        iconElementLeft={<LngMenu  />}
                        iconElementRight={isLogged ? <LoggedMenuContainer/> : null}
                        style={styles.title}
                    />
                  <ConfirmNavigationModalContainer/>
              </div>
    );
}

Header.propTypes = {
    fetching : PropTypes.number.isRequired,
    isLogged : PropTypes.bool.isRequired,
    t : PropTypes.func.isRequired,
}


export default  translate(['common'], { wait: true })( Header);



