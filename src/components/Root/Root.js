import React, { PropTypes } from 'react'
import { Provider } from 'react-redux'
import configRoutes from '../../routes'
import { Router } from 'react-router'

const Root = ({ store, history }) => {
    const routes= configRoutes(store);
    return (
        <Provider store={store}>
            <div>
                <Router history={history} routes={routes}/>
            </div>
        </Provider>
    )
}
Root.propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
}

export default Root;