/**
 * Created by IMAD on 19/04/2017.
 */

import React from "react";
import HeaderContainer from"./Header/HeaderContainer";
import ErrorMsgContainer from './Messages/ErrorMsgContainer'
import InformationMsgContainer from './Messages/InformationMsgContainer'
import styles from '../../css/layout.css';

// use dump component

export default function  Layout  ({children}){
    return(
        <div style={styles.columns}>
            <div  style={styles.titleColumn} >
                <HeaderContainer />
                <ErrorMsgContainer style={styles.fullWidth}/>
                <InformationMsgContainer style={styles.fullWidth}/>
            </div>
            <div id="children" style={styles.mainColumn} >
                {children}
            </div>
        </div>
    );
};