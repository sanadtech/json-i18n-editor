/**
 * Created by IMAD on 25/05/2017.
 */

import {connect} from "react-redux";
import DisplayMsg from "./DisplayMsg";
import {closeInfo} from '../../../actions/InfoActions'
import React from "react";

function InformationMsgContainer ({onClose,open, info}){
    return <DisplayMsg   onClose={onClose} open={open} message={info} type="info" />
}

const mapDispatchToProps = {
    onClose:closeInfo,
};

const mapStateToProp = (state)=>{
    return {
        open : state.info.open,
        info : state.info.info
    }
};


export default connect(mapStateToProp,mapDispatchToProps)(InformationMsgContainer);