/**
 * Created by IMAD on 26/04/2017.
 */
import React,{PropTypes} from 'react';
import Snackbar from 'material-ui/Snackbar';
import { translate } from 'react-i18next';
import styles from '../../../css/layout.css'

function DisplayMsg({t,open, message, type ,onClose}) {
    let style = (type==='error')?styles.failedMsg:styles.successfulMsg;
    return (message!==null) ?
        <Snackbar
            open={open}
            contentStyle={style}
            message={message}
            autoHideDuration={6000}
            action={t("close")}
            onRequestClose={onClose}
            onActionTouchTap={onClose}/>
        : null
}

/*
*
*
 * import Chip from 'material-ui/Chip';
 import Avatar from 'material-ui/Avatar';
 import SvgIconFace from 'material-ui/svg-icons/action/face';
  * <Chip
 onRequestDelete={onClose}
 color="#555"
 >
 <Avatar color="#666" icon={<SvgIconFace />} />
 {message}
 </Chip>
*
* */

DisplayMsg.PropTypes = {
    onClose: PropTypes.func.isRequired,
    message : PropTypes.string.isRequired,
    type : PropTypes.string.isRequired,
    open:PropTypes.bool.isRequired,
    t: PropTypes.func.isRequired
}
export default translate(['common'], { wait: true })(DisplayMsg);
