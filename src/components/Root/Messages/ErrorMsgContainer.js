/**
 * Created by IMAD on 26/04/2017.
 */


import {connect} from "react-redux";
import DisplayMsg from "./DisplayMsg";
import {closeError} from '../../../actions/ErrorActions'
import React from "react";

function ErrorMsgContainer ({onClose,open, error}){
    return <DisplayMsg   onClose={onClose} open={open} message={error} type="error" />
}

const mapDispatchToProps = {
    onClose:closeError,
};

const mapStateToProp = (state)=>{
    return {
        open : state.error.open,
        error : state.error.error
    }
};


export default connect(mapStateToProp,mapDispatchToProps)(ErrorMsgContainer);
