/**
 * Created by IMAD on 18/04/2017.
 */

import React   from 'react';
import {connect} from "react-redux";
import TranslatePage from './TranslatePage';
import {addHistoric} from "../../actions/HistoricActions";

function TranslatePageContainer ({addHistoric,mode}){
    return (
        <TranslatePage addHistoric={addHistoric} mode={mode} />
    );
}



const mapDispatchToProps = {
    addHistoric : addHistoric
};

const  mapStateToProp = (state)=>{
    return {
        mode : state.mode.mode,
    }
};

export default  connect(mapStateToProp,mapDispatchToProps)(TranslatePageContainer)