/**
 * Created by IMAD on 09/04/2017.
 */


import React,{PropTypes} from "react";
import TextField from 'material-ui/TextField';
import styles from '../../../css/layout.css'

export default class Input extends  React.Component{
    //  pass `props` from the `constructor` to the `super` function
    constructor(props){
        super(props);
        this.state={
            value :'-'
        }

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data.targetValue !== this.props.value) {
            this.setState({value: nextProps.data.targetValue });

        }
    }


    componentDidMount() {
        const value = this.props.data.targetValue ;
        this.setState({value});
    }

    //  handle what ? please use meaningful names
    handleChange(e){
        this.setState({value :e.target.value});
        const {onUpdate,data}= this.props;
        onUpdate(data.key,e.target.value);
    }

    render()  {
        return(
            <TextField hintText="targetValue" fullWidth={true} style={styles.tableColumn}  value={this.state.value}  onChange={(e)=>{this.handleChange(e)}} />
        );
    }
}

Input.propTypes = {
    onUpdate: PropTypes.func.isRequired,
    data:PropTypes.shape({
        key: PropTypes.string.isRequired,
        sourceValue: PropTypes.string,
        targetValue: PropTypes.string
    }).isRequired,
};
