/**
 * Created by IMAD on 30/03/2017.
 */

import {connect} from "react-redux";
import {getFilesFromGit} from "../../../actions/FilesActions";
import {updateKeyword,filterKeywords} from "../../../actions/keywordsActions";
import {openTranslationModal,openDeleteModal} from "../../../actions/modalActions";
import {onColumnResizeEndCallback} from "../../../actions/ColumnWidthActions";
import React from "react";
import  KeysTable from './KeysTable';

function KeysTableContainer ({filtredKeyword,lengthKeywords,filter,mode,fetching,onRefresh, onUpdate, onOpenDeleteModal,eltFiltered ,onFilter,onOpenTranslateModal,onColumnResizeEndCallback,columnWidths}){

        return(
          <KeysTable state={filtredKeyword} onUpdate={onUpdate}  onFilter={onFilter} lengthKeywords={lengthKeywords}
                     filter={filter} eltFiltered={eltFiltered} mode={mode}
                     onColumnResizeEndCallback={onColumnResizeEndCallback} columnWidths={columnWidths}
                     onOpenDeleteModal={onOpenDeleteModal} onOpenTranslateModal={onOpenTranslateModal}
                     fetching={fetching} onRefresh={onRefresh}
          />
        )
}

const mapDispatchToProps = {
    onUpdate : updateKeyword,
    onFilter : filterKeywords,
    onOpenDeleteModal: openDeleteModal,
    onOpenTranslateModal : openTranslationModal,
    onColumnResizeEndCallback : onColumnResizeEndCallback,
    onRefresh : getFilesFromGit
}

const  mapStateToProp = (state)=>{
    return {
        columnWidths: state.columnWidths,
        lengthKeywords : state.keywords.keywords.length,
        mode: state.mode.mode,
        filtredKeyword: state.keywords.filtredKeyword,
        filter:state.keywords.filter,
        eltFiltered:state.keywords.eltFiltered,
        fetching: state.load.queue.length,
    }
}

export default connect(mapStateToProp,mapDispatchToProps)(KeysTableContainer);