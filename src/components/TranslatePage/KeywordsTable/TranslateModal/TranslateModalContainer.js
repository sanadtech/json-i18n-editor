/**
 * Created by IMAD on 09/04/2017.
 */



import {connect} from "react-redux";
import {updateByIndexfromMdEditor} from "../../../../actions/keywordsActions";
import {closeTranslationModal} from '../../../../actions/modalActions';
import React from "react";
import  TranslateModal from './TranslateModal';


function  TranslateModalContainer ({open, onUpdate,closeModal,data,mode}){
    return  <TranslateModal open={open} onUpdate={onUpdate} closeModal={closeModal}
                            data={data} mode={mode}  />
}

const mapDispatchToProps = {
    closeModal : closeTranslationModal,
    onUpdate : updateByIndexfromMdEditor,
}

const  mapStateToProp = (state)=>{
    return{
        open : state.modal.translationModal.open ,
        data : state.modal.translationModal.data,
        mode : state.mode.mode
    }
}

export default connect(mapStateToProp,mapDispatchToProps)(TranslateModalContainer);