/**
 * Created by IMAD on 09/04/2017.
 */

import React,{PropTypes} from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { translate } from 'react-i18next';
import styles from '../../../../css/layout.css';
import MarkdownEditorTargetValueContainer from '../../MarkdownEditor/MarkdownEditorTargetValueContainer';
import MarkdownEditorSourceValueContainer from '../../MarkdownEditor/MarkdownEditorSourceValueContainer';



class TranslateModal extends React.Component {

    render() {
        const {open,closeModal,onUpdate,data,mode}=this.props;
        const actions = [
            <FlatButton
                label="Cancel"
                primary={false}
                onTouchTap={closeModal}
            />,
            <FlatButton
            label="OK"
            primary={true}
            onTouchTap={()=>{onUpdate(data.key);closeModal();}}
            />
        ];
        return (
            <Dialog
                    title={data.key}
                    actions={actions}
                    modal={true}
                    autoScrollBodyContent={true}
                    contentStyle={styles.modal}
                    open={open}
            >

               {
                   (mode==='translate')?
                       <MarkdownEditorSourceValueContainer/>
                       :null
               }
                <div style={{height:'300px'}}>
                    <MarkdownEditorTargetValueContainer/>
                </div>
            </Dialog>
        );
    }
}

TranslateModal.PropTypes = {
    open:PropTypes.bool.isRequired,
    data:PropTypes.shape({
        key: PropTypes.string,
        sourceValue: PropTypes.string,
        targetValue: PropTypes.string
    }).isRequired,
    mode: PropTypes.string.isRequired,
    closeModal:PropTypes.func.isRequired,
    onUpdate: PropTypes.func.isRequired,
}

export default  translate(['common'], { wait: true })(TranslateModal);