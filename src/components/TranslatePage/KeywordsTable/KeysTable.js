/**
 * Created by IMAD on 08/04/2017.
 */

import React,{PropTypes} from "react";
import {Table,Column, Cell} from 'fixed-data-table-2';
import { translate } from 'react-i18next';
import 'fixed-data-table-2/dist/fixed-data-table.css';
import Input from './Input';
import IconButton from 'material-ui/IconButton';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import ActionTranslate from 'material-ui/svg-icons/action/g-translate';
import ActionRefresh from "material-ui/svg-icons/navigation/refresh";
import styles from '../../../css/layout.css';
import TranslateModalContainer from './TranslateModal/TranslateModalContainer';
import DeleteModalContainer from './DeleteModal/DeleteModalContainer';
import Highlighter from 'react-highlight-words';
import Dimensions from 'react-dimensions'


const FilterHeader=({name,cellDataKey,onFilterChange,...props})=> {
    return (
        <Cell {...props}>
            <span>{name}</span> <br/>
            <input style={styles.fullWidth}  onChange={(e)=>onFilterChange(cellDataKey,e.target.value)}/>
        </Cell>
    );
};
const InputCell = ({rowIndex, data,onUpdate, ...props}) => (
        <Input onUpdate={onUpdate} data={data[rowIndex]} />
);

const TextCell = ({rowIndex, data, col,filter,eltFiltered, ...props}) => (
        <Cell {...props}>
            {(col===eltFiltered)?
                <Highlighter
                    highlightStyle={styles.highlight}
                    searchWords={[filter]}
                    textToHighlight={data[rowIndex][col]}
                />
            :
                data[rowIndex][col]
            }
        </Cell>
);

const OptionsCell = ( {rowIndex, data,onOpenTranslateModal,onOpenDeleteModal}) => {
    return (
        <div>
            <IconButton tooltip="Edit " onClick={()=>onOpenTranslateModal(true,data[rowIndex],rowIndex)}>
                <ActionTranslate />
            </IconButton>
            <IconButton tooltip="Delete " onClick={()=>onOpenDeleteModal(true,data[rowIndex].key)}>
                <ActionDelete />
            </IconButton>
        </div>
    );
}

class KeysTable extends React.Component{
    // use responsive table instead of fixed data table https://www.npmjs.com/package/responsive-fixed-data-table
    render(){
        const {containerHeight, containerWidth,lengthKeywords,fetching,onRefresh,mode,t,onUpdate, onOpenDeleteModal,onOpenTranslateModal,onFilter,state,onColumnResizeEndCallback,columnWidths,eltFiltered,filter,...props} =this.props;

        return (
            (fetching === 0 && lengthKeywords === 0) ?
                <div style={styles.container}>
                    <IconButton iconStyle={styles.largeIcon}
                                style={styles.large}
                                onTouchTap={onRefresh}
                                tooltip={t("Refresh")}
                                tooltipPosition="top-center">
                        <ActionRefresh/>
                    </IconButton>
                </div>
                :
                <div style={styles.table}>
                    <Table
                        rowHeight={70}
                        rowsCount={state.length}
                        width={containerWidth}
                        height={containerHeight}
                        headerHeight={60}
                        onColumnResizeEndCallback={onColumnResizeEndCallback}
                        isColumnResizing={false}
                        {...props}
                    >
                        <Column
                            header={<FilterHeader name={t("key")} cellDataKey="key" onFilterChange={onFilter}/>}
                            cell={<TextCell data={state} col="key" eltFiltered={eltFiltered} filter={filter}/>}
                            fixed={true}
                            width={columnWidths.key}
                            columnKey="key"
                            isResizable={true}
                            flexGrow={1}
                        />
                        {(mode === 'translate') ?
                            <Column
                                header={<FilterHeader name={t("sourceValue")} cellDataKey="sourceValue"
                                                      onFilterChange={onFilter}/>}
                                cell={<TextCell data={state} col="sourceValue" filter={filter}
                                                eltFiltered={eltFiltered}/>}
                                fixed={true}
                                width={columnWidths.sourceValue}
                                columnKey="sourceValue"
                                isResizable={true}
                                flexGrow={2}
                            /> : null}
                        <Column
                            header={<FilterHeader name={(mode === 'translate') ? t("targetValue") : t("Value")}
                                                  cellDataKey="targetValue" onFilterChange={onFilter}/>}
                            cell={<InputCell data={state} onUpdate={onUpdate}/>}
                            columnKey="targetValue"
                            fixed={true}
                            width={columnWidths.targetValue}
                            isResizable={true}
                            flexGrow={3}
                        />
                        <Column
                            header={<Cell>{t("options")}</Cell>}
                            columnKey="options"
                            cell={<OptionsCell data={state} onOpenTranslateModal={onOpenTranslateModal}
                                               onOpenDeleteModal={onOpenDeleteModal}/>}
                            fixed={true}
                            width={columnWidths.options}
                            isResizable={true}
                        />
                    </Table>
                    <TranslateModalContainer />
                    <DeleteModalContainer/>
                </div>
        );
    }
}

KeysTable.propTypes = {
    state: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string.isRequired,
        sourceValue: PropTypes.string,
        targetValue: PropTypes.string
    }).isRequired).isRequired,
    columnWidths: PropTypes.shape({
        key: PropTypes.number.isRequired,
        sourceValue: PropTypes.number.isRequired,
        targetValue: PropTypes.number.isRequired,
        options: PropTypes.number.isRequired,
    }).isRequired,
    lengthKeywords: PropTypes.number.isRequired,
    mode: PropTypes.string.isRequired,
    filter : PropTypes.string,
    eltFiltered : PropTypes.string,
    onOpenTranslateModal: PropTypes.func.isRequired,
    onOpenDeleteModal: PropTypes.func.isRequired,
    onUpdate: PropTypes.func.isRequired,
    onFilter: PropTypes.func.isRequired,
    onRefresh :  PropTypes.func.isRequired,
    fetching: PropTypes.number.isRequired,
    onColumnResizeEndCallback: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};

export default Dimensions({
    getHeight: ()=> {return window.innerHeight - 160;},
    getWidth: ()=> {let widthOffset = window.innerWidth < 680 ? 0 : 20;return window.innerWidth - widthOffset;}
})( translate(['common'], { wait: true })(KeysTable));