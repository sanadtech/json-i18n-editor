/**
 * Created by IMAD on 19/04/2017.
 */



import {connect} from "react-redux";
import {deleteKeyWord} from "../../../../actions/keywordsActions";
import {closeDeleteModal} from '../../../../actions/modalActions';
import React from "react";
import  DeleteModal from './DeleteModal';

function DeleteModalContainer ({key,open, onDelete,closeModal}){
    return <DeleteModal open={open} onDelete={onDelete}  closeModal={closeModal}    />
}


const mapDispatchToProps = {
    closeModal : closeDeleteModal,
    onDelete : deleteKeyWord,
};

const  mapStateToProp = (state)=>{
    return {
        open : state.modal.deleteModal.open,
    }
};

export default connect(mapStateToProp,mapDispatchToProps)(DeleteModalContainer);