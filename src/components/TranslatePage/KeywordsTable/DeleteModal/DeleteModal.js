/**
 * Created by IMAD on 09/04/2017.
 */

import React,{PropTypes} from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import styles from '../../../../css/layout.css';
import { translate } from 'react-i18next';





class DeleteModal extends React.Component {


    render() {
        const {open,closeModal,onDelete,t}=this.props;
        const actions = [
            <FlatButton label={t("delete")} primary={true}  style={styles.buttons} autoFocus={true} onTouchTap={()=>{onDelete();closeModal()}}/>,
            <FlatButton label={t("Cancel")} primary={false} onTouchTap={closeModal} />,
        ];
        return (
            <div>
                <Dialog
                    title={t("titleDeleteModal")}
                    modal={true}
                    open={open}
                    actions={actions}
                />
            </div>
        );
    }
}


DeleteModal.PropTypes = {
    open:PropTypes.bool.isRequired,
    closeModal:PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
}

export default  translate(['common'], { wait: true })(DeleteModal);
