/**
 * Created by IMAD on 17/05/2017.
 */


import React   from 'react';
import {connect} from "react-redux";
import MarkdownEditor from './MarkdownEditor';
import {changeMdEditorTargetValue} from '../../../actions/MarkdownEditorActions';

function MarkdownEditorContainer ({mdeValue,onChangeMdEditorValue}){


    return (
        <MarkdownEditor mdEditorValue={mdeValue} onChangeMdEditorValue={onChangeMdEditorValue}  type="newValue" disabled={false} rows={8} />
    );
}

const mapDispatchToProps = {
    onChangeMdEditorValue : changeMdEditorTargetValue,
};

const  mapStateToProp = (state)=>{
    return {mdeValue : state.markdownEditor.targetValue}
};


export default  connect(mapStateToProp,mapDispatchToProps)(MarkdownEditorContainer)