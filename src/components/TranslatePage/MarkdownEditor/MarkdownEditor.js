/**
 * Created by IMAD on 17/05/2017.
 */


import React,{PropTypes} from "react";
import {Tabs, Tab} from 'material-ui/Tabs';
import styles from '../../../css/layout.css';
import { translate } from 'react-i18next';
import TextField from 'material-ui/TextField';
import { Markdown } from 'react-showdown';


function  MarkdownEditor ({t,type,onChangeMdEditorValue , mdEditorValue , disabled , rows})  {

    return(
        <div style={styles.markdown} >
            <Tabs inkBarStyle={styles.black} tabItemContainerStyle={styles.tabItemContainerStyle}  >
                <Tab    label={t(type)}  >
                    <div style={styles.tabBorder}>
                        <TextField
                                hintText={t("EnterValue")}
                                multiLine={true}
                                rows={rows}
                                rowsMax={rows}
                                fullWidth={true}
                                value={mdEditorValue}
                                disabled={disabled}
                                onChange={event=>onChangeMdEditorValue(event.target.value)}
                            />
                        <a style={styles.yellowText}  href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet/" target="_blank">{t("markdownHelp")}</a>
                    </div>
                </Tab>
                <Tab label={t("Preview")}  >
                    <div  style={styles.preview}>
                        <Markdown markup={mdEditorValue} />
                    </div>
                </Tab>
            </Tabs>

        </div>
    );
}



MarkdownEditor.propTypes = {
    type: PropTypes.string.isRequired,
    mdEditorValue: PropTypes.string.isRequired,
    rows: PropTypes.number.isRequired,
    disabled: PropTypes.bool.isRequired,
    onChangeMdEditorValue : PropTypes.func.isRequired,
};

export default   translate(['common'], { wait: true })(MarkdownEditor);