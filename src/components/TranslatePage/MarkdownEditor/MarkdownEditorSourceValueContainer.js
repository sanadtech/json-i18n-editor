/**
 * Created by IMAD on 17/05/2017.
 */


import React   from 'react';
import {connect} from "react-redux";
import MarkdownEditor from './MarkdownEditor';
import {changeMdEditorSourceValue} from '../../../actions/MarkdownEditorActions';

function MarkdownEditorContainer ({mdeValue,onChangeMdEditorValue}){


    return (
        <MarkdownEditor mdEditorValue={mdeValue} onChangeMdEditorValue={onChangeMdEditorValue} type="sourceValue"  disabled={true} rows={5}/>
    );
}

const mapDispatchToProps = {
    onChangeMdEditorValue : changeMdEditorSourceValue,
};

const  mapStateToProp = (state)=>{
    return {mdeValue : state.markdownEditor.sourceValue}
};


export default  connect(mapStateToProp,mapDispatchToProps)(MarkdownEditorContainer)