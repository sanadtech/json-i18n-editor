/**
 * Created by IMAD on 30/03/2017.
 */
import {connect} from "react-redux";
import {fetchUnTranslateKeywords} from "../../../actions/keywordsActions";
import {downloadFileTarget} from "../../../actions/FilesActions";
import {openCommitModal,openAddKeyModal} from '../../../actions/modalActions';
import SettingForm from "./SettingForm";
import React from "react";

class SettingFormContainer extends React.Component {
    render(){
        const {stateLength,onDownload,onFetchUnTranslateKeywords,mode,onOpenCommitModal,onOpenAddKeyModal} =this.props;
        return(
            <SettingForm
                onFetchUnTranslateKeywords={onFetchUnTranslateKeywords} stateLength={stateLength}
                onDownload={onDownload} mode={mode} onOpenAddKeyModal={onOpenAddKeyModal} onOpenCommitModal={onOpenCommitModal} />
        )
    }
}

const mapDispatchToProps = {
    onDownload : downloadFileTarget,
    onOpenCommitModal : openCommitModal,
    onFetchUnTranslateKeywords:fetchUnTranslateKeywords,
    onOpenAddKeyModal:openAddKeyModal,
};

const  mapStateToProp = (state)=>{
    return {
        stateLength : state.keywords.keywords.length,
        mode : state.mode.mode,
    }
};



export default connect(mapStateToProp,mapDispatchToProps)(SettingFormContainer);