/**
 * Created by IMAD on 23/06/2017.
 */

import React,{PropTypes} from "react";
import styles from '../../../../css/layout.css'
import { translate } from 'react-i18next';


function AddedPart ({value}) {
    return (
            <span style={styles.successfulMsg} >{' + ' + value.key + ' :  ' + value.targetValue }</span>
    );
}

function  RemovedPart ({value}) {
    return (
        <span style={styles.failedMsg} > {' - ' + value.key + ' :  ' + value.targetValue }</span>
    );
}

function  ModifiedPart ({value}) {
    return (
        <span style={styles.modifiedMsg} > {' + ' + value.key + ' :  ' + value.targetValue }</span>
    );
}

function DiffComponent ({changes,t}){
    return(
            <div style={styles.DiffComponent} >
                {changes && changes.length>0 ?
                    changes.map(
                        (change,index)=> {
                            return  (change.added) ? <AddedPart key={index} value={change.value}/> :
                                (change.removed) ?  <RemovedPart  key={index} value={change.value}/> :
                                    (change.modified) ?     <ModifiedPart key={index} value={change.value}/> :null;
                        }
                    )
                    : t("noChanges")
                }
            </div>
    );
}

DiffComponent.propTypes = {
    changes:  PropTypes.array,
};

export default translate(['common'], { wait: true })(DiffComponent)