/**
 * Created by IMAD on 20/06/2017.
 */

import React,{PropTypes} from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { translate } from 'react-i18next';
import DiffComponent from './DiffComponent';



function  DiffModal ({open,closeModal,changes,t}) {

    let actions=[
        <FlatButton label={t("close")} primary={false} onTouchTap={()=>closeModal()}/>
    ];
    return (
        <div>
            <Dialog
                title={t("titleDiffModal")}
                modal={true}
                open={open}
                actions={actions}
                autoScrollBodyContent={true}
            >
                <DiffComponent changes={changes}/>
            </Dialog>
        </div>
    );
}

DiffModal.PropTypes = {
    open:PropTypes.bool.isRequired,
    changes:PropTypes.array
};

export default translate(['common'], { wait: true })(DiffModal)
