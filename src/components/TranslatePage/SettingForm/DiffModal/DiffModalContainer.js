/**
 * Created by IMAD on 19/04/2017.
 */



import {connect} from "react-redux";
import {closeDiffModal} from '../../../../actions/modalActions';
import React from "react";
import  DiffModal from './DiffModal';

function DiffModalContainer ({open,closeModal,changes}){
    return <DiffModal open={open} closeModal={closeModal}    changes={changes} />
}


const mapDispatchToProps = {
    closeModal : closeDiffModal,
};

const  mapStateToProp = (state)=>{
    return {
        changes : state.keywords.changes,
        open : state.modal.diffModal.open
    }
};

export default connect(mapStateToProp,mapDispatchToProps)(DiffModalContainer);