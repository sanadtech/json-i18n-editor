/**
 * Created by IMAD on 09/04/2017.
 */

import React,{PropTypes} from "react";
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { translate } from 'react-i18next';
import { Field, reduxForm } from 'redux-form';
import styles from '../../../../css/layout.css';
import MarkdownEditorContainer from '../../MarkdownEditor/MarkdownEditorTargetValueContainer';
import Dialog from 'material-ui/Dialog';

//do not use local state, keep one source of truth please => Redux


const validate = values => {
    const errors = {};
    const requiredFields = [ 'key' ];
    requiredFields.forEach(field => {
        if (!values[ field ] ) {
            errors[ field ] = 'Required'
        }
    });
    return errors;
};

const normalizeKey = (value,previousValue) => {
    if (!value) return value;
    return (!/^[a-zA-Z*/][a-zA-Z0-9_.[\]*/]*$/.test(value))?previousValue:value;
};



const renderTextField = ({ input, label ,meta: { touched, error  }, ...custom }) => (
    <TextField hintText={label}
               floatingLabelText={label}
               errorText={ touched && error}
               fullWidth={true}
               multiLine={false}
               {...input}
               {...custom}
    />
);

function AddForm ({open,closeModal,onAddSource,t,pristine,submitting, valid}) {
    const actions = [
        <RaisedButton
            style={styles.buttons}
            label="close"
            primary={false}
            onTouchTap={closeModal}
        />,
        <RaisedButton
            style={styles.buttons}
            label={t("addKey")}
            primary={false}
            disabled={pristine || !valid || submitting }
            type="submit"
            onTouchTap={onAddSource}
            autoFocus={true}
        />
    ];
    return(
        <div>
            <Dialog
                title={t("titleAddKeyModal")}
                modal={true}
                autoScrollBodyContent={true}
                contentStyle={styles.modal}
                open={open}
                actions={actions}
            >
                <form  onSubmit={(e)=>{e.preventDefault(); onAddSource()}}>
                    <Field   name="key" component={renderTextField} label={t("key")} normalize={normalizeKey} />
                    <MarkdownEditorContainer/>
                </form>
            </Dialog>
        </div>
    );
}

AddForm.propTypes = {
    onAddSource:  PropTypes.func.isRequired, //handleSubmit
    open:PropTypes.bool.isRequired,
    closeModal:PropTypes.func.isRequired,
};

export default  reduxForm({form: 'AddKeyForm', validate})(translate(['common','errors','infos'], { wait: true })(AddForm))