/**
 * Created by IMAD on 05/05/2017.
 */

import {connect} from "react-redux";
import {addKeySource} from "../../../../actions/keywordsActions";
import {closeAddKeyModal} from "../../../../actions/modalActions";
import AddForm from "./AddForm";
import React from "react";



function AddFormContainer ({onAddSource,open,added,error,closeModal}){
    return <AddForm  onAddSource={onAddSource} open={open} closeModal={closeModal} added={added} />
}

const mapDispatchToProps = {
    onAddSource: addKeySource,
    closeModal : closeAddKeyModal,
};

const  mapStateToProp = (state)=>{
    return {
        open : state.modal.addKeyModal.open
    }
};

export default connect(mapStateToProp,mapDispatchToProps)(AddFormContainer);
