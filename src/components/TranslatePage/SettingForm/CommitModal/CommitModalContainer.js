/**
 * Created by IMAD on 19/04/2017.
 */



import {connect} from "react-redux";
import {commitFileToGitlab} from "../../../../actions/FilesActions";
import {closeCommitModal,openDiffModal} from '../../../../actions/modalActions';


import React from "react";
import  CommitModal from './CommitModal';

function TranslateModalContainer ({fetching,open, onCommit,onCloseCommitModal,onOpenDiffModal}){
    return <CommitModal open={open} onCommit={onCommit}  onCloseCommitModal={onCloseCommitModal} onOpenDiffModal={onOpenDiffModal}  fetching={fetching} />
}


const mapDispatchToProps = {
    onCloseCommitModal : closeCommitModal,
    onCommit : commitFileToGitlab,
    onOpenDiffModal: openDiffModal,
};

const  mapStateToProp = (state)=>{
    return {
        fetching : state.load.queue.length,
        open : state.modal.commitModal.open
    }
};

export default connect(mapStateToProp,mapDispatchToProps)(TranslateModalContainer);