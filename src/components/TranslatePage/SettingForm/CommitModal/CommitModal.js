/**
 * Created by IMAD on 09/04/2017.
 */

import React,{PropTypes} from 'react';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import styles from '../../../../css/layout.css';
import { translate } from 'react-i18next';
import { Field, reduxForm } from 'redux-form';
import DiffModalContainer from '../DiffModal/DiffModalContainer';


//do not use local state, keep one source of truth please => Redux


const validate = values => {
    const errors = {};
    const requiredFields = [ 'message' ];
    requiredFields.forEach(field => {
        if (!values[ field ] ) {
            errors[ field ] = 'Required'
        }
    });
    return errors;
};

const renderTextField = ({ input, label, type ,meta: { touched, error }, ...custom }) => (
    <TextField hintText={label}
               floatingLabelText={label}
               errorText={touched && error}
               fullWidth={true}
               multiLine={true}
               type={type}
               {...input}
               {...custom}
    />
);


function  CommitModal ({open,onCloseCommitModal,fetching,onCommit,onOpenDiffModal,t,pristine,submitting, valid}) {
    let disabled= (fetching !==0) ;
    return (
        <div>
            <Dialog
                title={t("titleModal")}
                modal={true}
                open={open}
            >
                <form  onSubmit={(e)=>{e.preventDefault(); onCommit();}}>
                    <Field name="message" component={renderTextField} label={t("message")} type="input"/>
                    <RaisedButton
                        label={t("Diff")}
                        primary={false}
                        style={styles.buttons}
                        onTouchTap={() => onOpenDiffModal()}
                    />
                    <RaisedButton
                        label={t("Commit")}
                        primary={false}
                        style={styles.buttons}
                        disabled={pristine || !valid || submitting  || disabled}
                        type="submit"
                        autoFocus={true}
                    />
                    <RaisedButton
                        label={t("Cancel")}
                        primary={false}
                        disabled={submitting || disabled}
                        style={styles.buttons}
                        onTouchTap={()=>onCloseCommitModal()}
                    />
                    <DiffModalContainer/>
                </form>
            </Dialog>
        </div>
    );
}

CommitModal.PropTypes = {
    open:PropTypes.bool.isRequired,
    onCloseCommitModal:PropTypes.func.isRequired,
    onCommit: PropTypes.func.isRequired,
    onOpenDiffModal: PropTypes.func.isRequired,
    fetching : PropTypes.number.isRequired,
}

export default  reduxForm({form: 'commitForm', validate})(translate(['common'], { wait: true })(CommitModal))
