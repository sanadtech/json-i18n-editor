/**
 * Created by IMAD on 30/03/2017.
 */


import React,{PropTypes} from "react";
import RaisedButton from 'material-ui/RaisedButton';
import styles from '../../../css/layout.css';
import { translate } from 'react-i18next';
import CommitModalContainer from './CommitModal/CommitModalContainer';
import AddFormContainer from './AddKeyModal/AddFormContainer';
import Toggle from 'material-ui/Toggle';

function SettingForm( {onOpenCommitModal,onOpenAddKeyModal,onDownload,onFetchUnTranslateKeywords,stateLength,mode,t}){

    return (
            <div style={styles.row}>
                <div style={styles.elt}>
                    <RaisedButton disabled={(stateLength===0)} label={t("Commit")} primary={false} onTouchTap={() => onOpenCommitModal()}/>
                    <CommitModalContainer/>
                </div>
                <div style={styles.elt}>
                    <RaisedButton disabled={(stateLength===0)} label={t("Download")} primary={false} onTouchTap={() => onDownload()} />
                </div>
                {
                    (mode==='write')?
                        <div style={styles.elt}>
                            <RaisedButton disabled={(stateLength===0)} label={t("AddKey")} primary={false} onTouchTap={()=>onOpenAddKeyModal()}/>
                            <AddFormContainer/>
                        </div>
                    :
                        <div style={styles.elt}>
                            <Toggle disabled={(stateLength===0)} style={styles.toggle} label={t("UNTRANSLATABLE_KEYWORDS")} defaultToggled={false}   onToggle={(e,isChecked)=>onFetchUnTranslateKeywords(isChecked)} />
                        </div>
                }

            </div>
    );

}

SettingForm.propTypes = {
    onOpenCommitModal: PropTypes.func.isRequired,
    onOpenAddKeyModal: PropTypes.func.isRequired,
    onDownload :PropTypes.func.isRequired,
    onFetchUnTranslateKeywords :PropTypes.func.isRequired,
    stateLength: PropTypes.number.isRequired,
    mode: PropTypes.string.isRequired,
}


export default translate(['common'], { wait: true })(SettingForm);