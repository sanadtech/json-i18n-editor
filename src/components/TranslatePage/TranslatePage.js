/**
 * Created by IMAD on 18/04/2017.
 */
import React, {Component,PropTypes} from 'react';
import SettingFormContainer from './SettingForm/SettingFormContainer';
import KeysTableContainer from "./KeywordsTable/KeysTableContainer";
import styles from '../../css/layout.css';


class TranslatePage extends Component {


    componentDidMount() {
        const {addHistoric,mode} = this.props;
        if(mode==='translate') addHistoric();
    }


    render() {
        return (
           <div style={styles.container} >
               <div  style={styles.fixed}>
                   <SettingFormContainer/>
               </div>
               <div  style={styles.afterFixed}>
                   <KeysTableContainer/>
               </div>
           </div>
        );
    }
}

TranslatePage.propTypes = {
    addHistoric:PropTypes.func.isRequired,
    mode: PropTypes.string.isRequired,
};


export default TranslatePage;



