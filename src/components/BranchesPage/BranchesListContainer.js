/**
 * Created by IMAD on 19/04/2017.
 */

import {connect} from "react-redux";
import {addBranch, fetchBranches,filterBranches} from "../../actions/branchesActions";
import React from "react";
import BranchesList from "./BranchesList";

function BranchesListContainer({onFetchBranches, branches,branchesLength, fetching, onSelectBranch,onFilterBranches}) {
    return <BranchesList fetching={fetching} branches={branches}
                         onFetchBranches={onFetchBranches} onSelectBranch={onSelectBranch}
                         onFilterBranches={onFilterBranches} branchesLength={branchesLength}/>
}

const mapDispatchToProps = {
    onFetchBranches : fetchBranches,
    onFilterBranches : filterBranches,
    onSelectBranch : addBranch,
};

const  mapStateToProp = (state)=>{
    return {
        branches : state.branches.filteredBranches,
        branchesLength : state.branches.branches.length,
        fetching: state.load.queue.length,
    }
};

export default connect(mapStateToProp,mapDispatchToProps)(BranchesListContainer);