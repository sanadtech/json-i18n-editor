/**
 * Created by IMAD on 19/04/2017.
 */
import React, {PropTypes} from "react";
import {translate} from "react-i18next";
import {List, ListItem} from "material-ui/List";
import ActionInfo from "material-ui/svg-icons/action/info";
import ActionRefresh from "material-ui/svg-icons/navigation/refresh";
import IconButton from "material-ui/IconButton";
import Subheader from "material-ui/Subheader";
import Avatar from "material-ui/Avatar";
import FileFolder from "material-ui/svg-icons/file/folder";
import styles from "../../css/layout.css";
import SearchForm from '../MainPage/SearchForm/SearchForm'



class BranchesList extends React.Component {

    componentDidMount() {
        this.props.onFetchBranches() ;
    }

    render() {
        const {t, branches, fetching, branchesLength, onSelectBranch, onFetchBranches, onFilterBranches} = this.props;
        let i = 0 ;
        return (
            <div style={styles.container} >
                <Subheader style={styles.header} inset={true} >
                    <h4 style={{marginTop:50}}> {t("Branches")}</h4>
                    <SearchForm handleSubmit={onFilterBranches} fetching={fetching}/>
                </Subheader>
                <List style={styles.list}>
                    {(fetching === 0 && branchesLength === 0) ?
                        <div style={styles.container}>
                            <IconButton iconStyle={styles.largeIcon}
                                        style={styles.large}
                                        onTouchTap={onFetchBranches}
                                        tooltip={t("Refresh")}
                                        tooltipPosition="top-center">
                                <ActionRefresh/>
                            </IconButton>
                        </div>
                        :
                        branches.map(
                            branch => <ListItem
                                style={(i%2===1)? styles.oddLine : styles.pairLine}
                                key={i++}
                                leftAvatar={<Avatar icon={<FileFolder />}/>}
                                rightIcon={<ActionInfo />}
                                primaryText={branch.name}
                                secondaryText={<span> {branch.commit.message} <br/>  {new Date(branch.commit.created_at).toUTCString()} </span>}
                                onTouchTap={() => onSelectBranch(branch.name)}
                            />
                        )}
                </List>
            </div>
        );
    }
}

BranchesList.propTypes = {
    branches: PropTypes.arrayOf(PropTypes.shape({
        name:PropTypes.string.isRequired,
    }).isRequired).isRequired,
    fetching: PropTypes.number.isRequired,
    branchesLength: PropTypes.number.isRequired,
    onFetchBranches: PropTypes.func.isRequired,
    onSelectBranch:PropTypes.func.isRequired,
    onFilterBranches:PropTypes.func.isRequired,
};

export default  translate(['common'], { wait: true })(BranchesList);