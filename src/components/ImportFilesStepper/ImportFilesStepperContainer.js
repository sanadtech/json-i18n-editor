/**
 * Created by IMAD on 06/04/2017.
 */
import React from "react";
import {changeMode} from "../../actions/modeActions";
import {onResetFiles} from "../../actions/FilesActions";
import {connect} from "react-redux";
import ImportFilesStepper from "./ImportFilesStepper";
import {fetchTree} from "../../actions/treeActions";


function ImportFilesStepperContainer({fetching, treeLength, onFetchTree, onChangeMode, onReset, pathSource, pathTarget, mode}) {
    return (
            <ImportFilesStepper
                treeLength={treeLength} onFetchTree={onFetchTree}
                onChangeMode={onChangeMode} onReset={onReset} fetching={fetching}
                pathSource={pathSource} pathTarget={pathTarget} mode={mode}/>
    );
}


const mapDispatchToProps = {
    onChangeMode: changeMode,
    onReset : onResetFiles,
    onFetchTree : fetchTree,
};

const  mapStateToProp = (state)=>{
    return {
        pathSource : state.files.sourceFile.path,
        pathTarget : state.files.targetFile.path,
        treeLength: state.tree.list.length,
        fetching: state.load.queue.length,
        mode : state.mode.mode,
    }
};

export default  connect(mapStateToProp,mapDispatchToProps)(ImportFilesStepperContainer)