/**
 * Created by IMAD on 19/04/2017.
 */

import {connect} from "react-redux";
import {addTargetFile} from "../../../actions/FilesActions";
import React from "react";
import { translate } from 'react-i18next';
import ImportFileContainer from "./ImportFileContainer";



function ImportFileTargetContainer ( {onAddTargetFile,mode,path,fetching,nbrUntransKeys,t}){
    const disable = (mode!=='translate');
    return(
        <ImportFileContainer  onSelectFile={onAddTargetFile} header={t("AddNewLng")} path={path}
                     disable={disable}  nbrEmptyKeys={nbrUntransKeys}  fetching={fetching}
        />
    )
}


const mapDispatchToProps = {
    onAddTargetFile:addTargetFile,
};

const  mapStateToProp = (state)=>{
    return {
        mode : state.mode.mode,
        nbrUntransKeys : state.keywords.nbrUntransKeys,
        fetching : state.load.queue.length,
        path : state.files.targetFile.path,
    }
};

export default connect(mapStateToProp,mapDispatchToProps)( translate(['common'], { wait: true })(ImportFileTargetContainer));