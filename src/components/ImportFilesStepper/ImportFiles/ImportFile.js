/**
 * Created by IMAD on 19/04/2017.
 */
import React,{PropTypes} from "react";
import { translate } from 'react-i18next';
import TreeContainer from '../Tree/TreeContainer';
import styles from '../../../css/layout.css';
import Paper from 'material-ui/Paper';
import LinearProgress from 'material-ui/LinearProgress';

class ImportFile extends React.Component{

    componentDidMount() {
    }

    render(){
        const {header,onSelectFile,disable,path,nbrKeys,nbrEmptyKeys,fetching,error,t} = this.props;
        const style = (disable)? styles.disabled : styles.container;
        return (
            <Paper style={style}>
                <h4>{header}</h4>
                {
                    (path===''  || fetching!==0 || error !== null)?
                        <TreeContainer  onSelectFile={onSelectFile} disable={disable} />
                        :
                        <div style={styles.fullWidth}>
                            <div style={styles.Progress}>
                                {t('file')+ path}
                            </div>
                            <div style={styles.Progress}>
                                {t('nbrKeys') + nbrKeys + t('keys')}
                                <LinearProgress mode="determinate" value={nbrKeys} max={nbrKeys} min={0} />
                            </div>
                            <div style={styles.Progress}>
                                {t('emptyKeys') + nbrEmptyKeys + t('keys')}
                                <LinearProgress mode="determinate" value={nbrEmptyKeys} max={nbrKeys} min={0}/>
                            </div>
                        </div>
                }
            </Paper>
        );
    }

}


ImportFile.propTypes = {
    onSelectFile:PropTypes.func.isRequired,
    header:PropTypes.string.isRequired,
    disable : PropTypes.bool.isRequired,
    error:PropTypes.string,
    fetching: PropTypes.number.isRequired,
    path: PropTypes.string,
    nbrEmptyKeys : PropTypes.number.isRequired,
    nbrKeys  : PropTypes.number.isRequired,
}

export default  translate(['common'], { wait: true })(ImportFile);