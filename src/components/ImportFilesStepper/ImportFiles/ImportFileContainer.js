/**
 * Created by IMAD on 26/05/2017.
 */

import {connect} from "react-redux";
import React,{PropTypes} from "react";
import ImportFile from "./ImportFile";


function ImportFileContainer ( {onSelectFile,header,disable,nbrEmptyKeys,fetching,nbrKeys,path,error}){
    return(
        <ImportFile
            onSelectFile={onSelectFile} nbrEmptyKeys={nbrEmptyKeys}
            header={header} disable={disable} fetching={fetching}
            path={path}  error={error} nbrKeys={nbrKeys}
        />
    )
}

ImportFileContainer.propTypes = {
    onSelectFile:PropTypes.func.isRequired,
    header:PropTypes.string.isRequired,
    path:PropTypes.string.isRequired,
    disable : PropTypes.bool.isRequired,
    nbrEmptyKeys : PropTypes.number.isRequired,
    fetching: PropTypes.number.isRequired,
};



const  mapStateToProp = (state)=>{
    return {
        nbrKeys : state.keywords.keywords.length,
        error : state.error.error
    }
};


export default connect(mapStateToProp,null)(ImportFileContainer);