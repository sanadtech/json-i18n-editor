/**
 * Created by IMAD on 19/04/2017.
 */

import {connect} from "react-redux";
import {addSourceFile} from "../../../actions/FilesActions";
import React from "react";
import { translate } from 'react-i18next';
import ImportFileContainer from "./ImportFileContainer";


function ImportFileSourceContainer ( {onAddSourceFile,nbrEmptyKeys,path,t}){
    return(
            <ImportFileContainer
                nbrEmptyKeys={nbrEmptyKeys} onSelectFile={onAddSourceFile}
                header={t("AddRefLng")} disable={false} fetching={0} path={path}
            />
    )
}

const mapDispatchToProps = {
    onAddSourceFile:addSourceFile,
};

const  mapStateToProp = (state)=>{
    return {
        nbrEmptyKeys : state.keywords.nbrEmptyKeys,
        path : state.files.sourceFile.path,
    }
};


export default connect(mapStateToProp,mapDispatchToProps)( translate(['common'], { wait: true })(ImportFileSourceContainer));