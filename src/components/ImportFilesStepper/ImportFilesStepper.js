/**
 * Created by IMAD on 06/04/2017.
 */
import React, {PropTypes} from "react";
import ImportFileTargetContainer from "./ImportFiles/ImportFileTargetContainer";
import ImportFileSourceContainer from "./ImportFiles/ImportFileSourceContainer";
import {translate} from "react-i18next";
import styles from "../../css/layout.css";
import {RadioButton, RadioButtonGroup} from "material-ui/RadioButton";
import RaisedButton from "material-ui/RaisedButton";
import {browserHistory} from "react-router";
import ActionRefresh from "material-ui/svg-icons/navigation/refresh";
import IconButton from "material-ui/IconButton";

class ImportFilesStepper extends React.Component{

    componentDidMount() {
        const {onChangeMode,onFetchTree,onReset} = this.props;
        onChangeMode('write');
        onReset();
        onFetchTree();
    }

    render () {
        const {fetching, onChangeMode, onReset, pathSource, mode, pathTarget, onFetchTree, treeLength, t} = this.props;
        return (
            <div style={styles.container} >
                <div style={styles.fixedContainer}>
                    <div style={styles.formLogin}>
                        <RadioButtonGroup name={mode} style={styles.row}  defaultSelected="write" onChange={(e,value)=>onChangeMode(value)}>
                            <RadioButton
                                value="write"
                                label={t("write")}
                                style={styles.eltRow}
                            />
                            <RadioButton
                                value="translate"
                                label={t("translate")}
                                style={styles.eltRow}
                            />
                        </RadioButtonGroup>
                    </div>
                    <div style={styles.row}>
                        <div style={styles.elt}>
                            <RaisedButton label={t("back")} primary={true} type="submit" style={styles.buttons}
                                        onTouchTap={() => onReset()}  disabled={(pathSource === '') }/>
                            <RaisedButton label={t("next")} primary={true} type="submit" style={styles.buttons}
                                        onTouchTap={() => browserHistory.push('/projects/branches/files/translate')}
                                         disabled={(pathTarget === '' && mode === 'translate')||(pathSource === '') }/>
                        </div>
                    </div>
                </div>
                <div style={styles.afterFixedContainer}>
                {
                    (fetching === 0 && treeLength === 0) ?
                        <div style={styles.container}>
                            <IconButton iconStyle={styles.largeIcon}
                                        style={styles.large}
                                        onTouchTap={onFetchTree}
                                        tooltip={t("Refresh")}
                                        tooltipPosition="top-center">
                                <ActionRefresh/>
                            </IconButton>
                        </div>
                        :
                        <div style={styles.row}>
                            <div style={styles.eltRow}>
                                <ImportFileSourceContainer/>
                            </div>
                            <div style={styles.eltRow}>
                                <ImportFileTargetContainer />
                            </div>
                        </div>
                }
                </div>
            </div>
        );
    }
}

ImportFilesStepper.propTypes = {
    onChangeMode : PropTypes.func.isRequired,
    onFetchTree: PropTypes.func.isRequired,
    onReset : PropTypes.func.isRequired,
    treeLength: PropTypes.number.isRequired,
    pathTarget : PropTypes.string.isRequired,
    pathSource : PropTypes.string.isRequired,
    mode: PropTypes.string.isRequired,
    fetching: PropTypes.number.isRequired,
};
export default  translate(['common'], { wait: true })(ImportFilesStepper);