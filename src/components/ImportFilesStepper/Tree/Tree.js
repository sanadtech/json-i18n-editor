/**
 * Created by IMAD on 19/04/2017.
 */
import React,{PropTypes} from "react";
import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import FileFolder from 'material-ui/svg-icons/file/folder';
import ActionAssignment from 'material-ui/svg-icons/action/assignment';
import {blue500,yellow600} from 'material-ui/styles/colors';
import styles from '../../../css/layout.css';

class Tree extends React.Component {

    getChildren = (children,disable)=>{
        return children.map( node =>
            <ListItem
                key={node.uid ? node.uid : node.id}
                leftAvatar={<Avatar
                    icon={ node.type === "tree" ? <FileFolder /> : <ActionAssignment  />}
                    backgroundColor={ node.type === "tree" ? yellow600 : blue500  }
                />}
                disabled={disable}
                style={{zIndex: 0} }
                primaryText={node.name}
                secondaryText={node.path}
                initiallyOpen={false}
                primaryTogglesNestedList={true}
                autoGenerateNestedIndicator={true}
                onNestedListToggle={(e)=>this.selectChildren(e,node)}
                insetChildren={true}
                nestedItems={(node.type==="tree")? this.getChildren(node.children.list):[]}
            />
        )
    };

    selectChildren=(e,node)=>{
        const {onFetchChildren,onSelectFile}=this.props;
        if(node.type==="tree") {
            if(e.state.open) {
                if(node.children.list.length===0){
                    onFetchChildren(node.path);
                }
            }
        }else{
            onSelectFile(node.id,node.path,node.name);
        }
    }

    render(){
        const {tree,disable}=this.props;
        return(
                <List style={styles.list} >
                        {this.getChildren(tree ,disable)}
                </List>
        );
    }
}

Tree.propTypes = {
    tree: PropTypes.arrayOf(PropTypes.shape({
        id:PropTypes.string.isRequired,
        name:PropTypes.string.isRequired,
        type:PropTypes.string.isRequired,
        path: PropTypes.string.isRequired,
        children:PropTypes.shape({
            list:PropTypes.array,
            pathname:PropTypes.string
        }),
    }).isRequired).isRequired,
    disable:PropTypes.bool.isRequired,
    pathname:PropTypes.string.isRequired,
    onFetchChildren: PropTypes.func.isRequired,
    onSelectFile:PropTypes.func.isRequired
}

export default  Tree;
