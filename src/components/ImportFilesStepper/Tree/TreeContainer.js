/**
 * Created by IMAD on 26/04/2017.
 */

import {connect} from "react-redux";
import { fetchChildren} from "../../../actions/treeActions";
import React,{PropTypes} from "react";
import Tree from "./Tree";

class TreeContainer extends React.Component{
    render(){
        const {onFetchChildren,onSelectFile,tree,path,disable} =this.props;
        return(
          <Tree tree={tree} pathname={path} onFetchChildren={onFetchChildren}
                onSelectFile={onSelectFile} disable={disable} />
        )
    }
}

TreeContainer.propTypes = {
    onSelectFile:PropTypes.func.isRequired,
    disable : PropTypes.bool.isRequired,
};


const mapDispatchToProps = {
    onFetchChildren : fetchChildren
};

const  mapStateToProp = (state)=>{
    return {
        tree : state.tree.list,
        path: state.tree.pathname,
    }
}

export default connect(mapStateToProp,mapDispatchToProps)(TreeContainer);