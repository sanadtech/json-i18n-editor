/**
 * Created by IMAD on 18/04/2017.
 */

import {GetBranches} from '../utils/GitLabAPI';
import ActionTypes from '../constants/ActionTypes';
import { push } from 'react-router-redux';

export function fetchBranches(){
    return (dispatch,getState)=>{
        dispatch({type: ActionTypes.FETCH_BRANCHES_PENDING, fetching: true});
        GetBranches(
            getState().login.privateToken,
            getState().files.projectId,
            (result)=> dispatch({type: ActionTypes.FETCH_BRANCHES_FULFILLED, branches: result ,fetching:false}),
            (error)=> dispatch({type: ActionTypes.FETCH_BRANCHES_REJECTED, error, fetching:false })
        );
    }
}

export function filterBranches(query){
    return {type : ActionTypes.FILTER_BRANCHES, query}
}

export function addBranch(branch){
    return (dispatch)=> {
        dispatch({type : ActionTypes.ADD_BRANCH, payload : branch });
        dispatch(push('/projects/branches/files'));
    }
}







