/**
 * Created by IMAD on 06/04/2017.
 */

import ActionTypes from '../constants/ActionTypes';

export function addKeySource() {
    return (dispatch,getState) =>{
        const {key} = getState().form.AddKeyForm.values;
        const {targetValue}  = getState().markdownEditor;
        dispatch({type: ActionTypes.FIND_KEYWORD,key });
        if( getState().keywords.error.status ===null)  dispatch({type: ActionTypes.ADD_KEYWORD,key,targetValue,info:1});
        else dispatch({type : ActionTypes.ADD_ERROR,error:{status:100},key})
    }
}



export function updateByIndexfromMdEditor(key) {
    return (dispatch,getState) =>{
        const {targetValue} = getState().markdownEditor;
        dispatch({ type: ActionTypes.UPDATE_KEYWORD,  key,targetValue })
    }
}


export function fetchUnTranslateKeywords(checked) {
    return { type: ActionTypes.UNTRANSLATABLE_KEYWORDS, checked }
}

export function updateByIndex(index, targetValue) {
    return { type: ActionTypes.UPDATE_BY_INDEX,  index,targetValue }
}

export function updateKeyword(key, targetValue) {
    return { type: ActionTypes.UPDATE_KEYWORD,  key,targetValue }
}

export function deleteKeyWord() {
    return (dispatch,getState) =>{
        const {key} = getState().modal.deleteModal;
        dispatch({type: ActionTypes.DELETE_KEYWORD, key,info:3})
    }

}

export function filterKeywords(elt,query){
    return {type : ActionTypes.FILTER_KEYWORDS, elt, query}
}

export function GetNbrUntransKeys() {
    return {type: ActionTypes.NBR_UNTRANSLATABLE_KEYWORDS,payload:null}
}