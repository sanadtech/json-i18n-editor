/**
 * Created by IMAD on 26/04/2017.
 */

import ActionTypes from '../constants/ActionTypes';



export function closeError() {
    return {
        type: ActionTypes.CLOSE_ERROR,
        payload: false,
    }
}