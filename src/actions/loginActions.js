/**
 * Created by IMAD on 17/04/2017.
 */

import ActionTypes from '../constants/ActionTypes';
import { replace } from 'react-router-redux';
import {getProfile} from '../utils/GitLabAPI';


export function setTokenUsername(){
    return (dispatch)=>{
        dispatch( {
            type: ActionTypes.SET_TOKEN_USERNAME,
            privateToken: window.localStorage.getItem('privateToken'),
            username:window.localStorage.getItem('username'),
            host:window.localStorage.getItem('host'),
            isLogged:true,
        });
        dispatch( {
            type: ActionTypes.ADD_HOST,
            host:window.localStorage.getItem('host')
        });
    }
}


export function logout(){
    return (dispatch)=>{
        window.localStorage.removeItem('privateToken');
        window.localStorage.removeItem('username');
        window.localStorage.removeItem('expirateAt');
        window.localStorage.removeItem('host');
        dispatch( {type:ActionTypes.INITIAL_STATE,payload:null});
        dispatch(replace('/'));
    }

}

export function login(e){
    e.preventDefault();
    return (dispatch,getState)=>{
        dispatch({type: ActionTypes.LOGIN_START, fetching: true});
        const {username,privateToken,host} = getState().form.LoginForm.values;
        window.localStorage.setItem('host', host);
        getProfile(
            privateToken,
            username,
            (result)=>{
                if(result.length === 1){
                    window.localStorage.setItem('privateToken', privateToken);
                    window.localStorage.setItem('username', username);
                    window.localStorage.setItem('avatar', result[0].avatar_url);
                    if( JSON.parse(window.localStorage.getItem('historic'))===null) window.localStorage.setItem('historic', JSON.stringify([]));
                    let expirateDate=new Date();
                    expirateDate=new Date(expirateDate.setHours(expirateDate.getHours()+2));
                    window.localStorage.setItem('expirateAt',expirateDate);
                    // put privateToken in the login reducer => reuse it from there whenever you need it
                    dispatch({type: ActionTypes.LOGIN_FULLFILLED, privateToken,username , fetching:false });
                }else{
                    dispatch({type: ActionTypes.LOGIN_REJECT, error:{status : 430 }, fetching:false });

                }

            },
            (error)=>{

                dispatch({type: ActionTypes.LOGIN_REJECT, error, fetching:false });
            }
        );
    }
}