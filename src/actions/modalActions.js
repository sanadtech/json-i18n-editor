/**
 * Created by IMAD on 09/04/2017.
 */


import ActionTypes from '../constants/ActionTypes';

export function openCommitModal() {
    return {
        type: ActionTypes.OPEN_COMMIT_MODAL,
        open: true,
    }
}

export function closeCommitModal(){
    return {
        type: ActionTypes.CLOSE_COMMIT_MODAL,
        open: false,
    }
}

export function openDiffModal() {
    return {
        type: ActionTypes.OPEN_DIFF_MODAL,
        open: true,
    }
}

export function closeDiffModal(){
    return {
        type: ActionTypes.CLOSE_DIFF_MODAL,
        open: false,
    }
}

export function openAddKeyModal() {
    return {
        type: ActionTypes.OPEN_ADD_KEY_MODAL,
        open: true,
    }
}

export function closeAddKeyModal(){
    return {
        type: ActionTypes.CLOSE_ADD_KEY_MODAL,
        open: false,
    }
}

export function openNavigationModal(newPage) {
    return {
        type: ActionTypes.OPEN_NAVIGATION_MODAL,
        open:true,
        newPage
    }
}

export function closeNavigationModal(){
    return {
        type: ActionTypes.CLOSE_NAVIGATION_MODAL,
        open: false,
    }
}

export function openTranslationModal(open , data , indexRow) {
    return {
        type: ActionTypes.OPEN_TRANSLATION_MODAL,
        open , data , indexRow
    }
}

export function closeTranslationModal(){
    return {
        type: ActionTypes.CLOSE_TRANSLATION_MODAL,
        open: false,
    }
}

export function openDeleteModal(open , key) {
    return {type: ActionTypes.OPEN_DELETE_MODAL, open , key}
}

export function closeDeleteModal(){
    return {type: ActionTypes.CLOSE_DELETE_MODAL, open: false}
}


