/**
 * Created by IMAD on 17/05/2017.
 */

import ActionTypes from '../constants/ActionTypes';

export function changeMdEditorTargetValue(mdeValue) {
    return {
        type: ActionTypes.EDIT_MARKDOWN_TARGET_VALUE,
        targetValue : mdeValue,
    }
}


export function changeMdEditorSourceValue(mdeValue) {
    return {
        type: ActionTypes.EDIT_MARKDOWN_SOURCE_VALUE,
        sourceValue : mdeValue,
    }
}
