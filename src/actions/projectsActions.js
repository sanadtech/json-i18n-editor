/**
 * Created by IMAD on 17/04/2017.
 */
import {GetProjects} from '../utils/GitLabAPI';
import ActionTypes from '../constants/ActionTypes';
import { push } from 'react-router-redux';

export function fetchProjects(){
    return (dispatch, getState)=>{
        dispatch({type: ActionTypes.FETCH_PROJECTS_PENDIND ,fetching: true});
        GetProjects(
            // get privateToken from login state/reducer instead
            getState().login.privateToken,
            result=>dispatch({type: ActionTypes.FETCH_PROJECTS_FULFILLED, projects: result,fetching:false}),
            (error)=>{
                //  store the error in the projects state
                //  remove ActionTypes.FETCH_END action
                dispatch({type: ActionTypes.FETCH_PROJECTS_REJECTED, error, fetching:false });
            }
        );
    }
}

export function filterProjects(query){
    return {type : ActionTypes.FILTER_PROJECTS, query}
}

export function addProject(projectId,projectName){
    return (dispatch)=> {
        dispatch({type : ActionTypes.ADD_PROJECT,  projectId ,projectName });
        dispatch(push('/projects/branches'));
    }
}