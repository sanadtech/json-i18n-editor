/**
 * Created by IMAD on 06/04/2017.
 */

import ActionTypes from '../constants/ActionTypes';

export function changeMode(mode) {
    return {
        type: ActionTypes.CHANGE_MODE,
        mode,
    }
}

export function openForm() {
    return {
        type: ActionTypes.OPEN_FORM,
        open: true,
    }
}

export function openDropDown(anchorEl) {
    return {
        type: ActionTypes.OPEN_DROPDOWN,
        open: true,
        anchorEl,
    }
}

export function closeForm() {
    return {
        type: ActionTypes.CLOSE_FORM,
        open: false,
    }
}

