/**
 * Created by IMAD on 04/05/2017.
 */


import ActionTypes from "../constants/ActionTypes";

export function setHistoric(){
    return (dispatch,getState)=> {
        let host = getState().files.host;
        let Historic =JSON.parse(window.localStorage.getItem('historic'));
        let newHistoric= Historic.filter(elt=> elt.host === host);
        if(newHistoric.length>3) newHistoric = newHistoric.slice(0,3);
        return dispatch({ type: ActionTypes.FETCH_HISTORIC, historic: newHistoric})
    }
}

export function deleteHistoric(files) {
    let lastHistoric = JSON.parse(window.localStorage.getItem('historic'));
    let newHistoric = lastHistoric.filter(elt =>
        !( elt.projectName === files.projectName
        && elt.branch === files.branch
        && elt.targetFile.path === files.targetFile.path
        && elt.sourceFile.path === files.sourceFile.path)
    );
    window.localStorage.setItem("historic", JSON.stringify(newHistoric));
    return setHistoric();
}


export function addHistoric(){
    return (dispatch,getState)=> {
        let newFiles = getState().files;
        let lastHistoric =JSON.parse(window.localStorage.getItem('historic'));
        let newHistoric= lastHistoric.filter(elt=>
            !(elt.projectName === newFiles.projectName
            && elt.branch ===  newFiles.branch
            && elt.targetFile.path ===  newFiles.targetFile.path
            && elt.sourceFile.path ===  newFiles.sourceFile.path)
         );
        newHistoric=[getState().files,...newHistoric];
        window.localStorage.setItem("historic",JSON.stringify(newHistoric));
        return setHistoric();
    }
}