/**
 * Created by IMAD on 06/04/2017.
 */

import ActionTypes from '../constants/ActionTypes';
import { push } from 'react-router-redux';

export function nextStep() {
    return {
        type: ActionTypes.NEXT_STEP,
        payload: null,
    }
}

export function prevStep(){
    return {
        type: ActionTypes.PREV_STEP,
        payload: null,
    }
}

export function setStep(stepIndex) {
    return {
        type: ActionTypes.SET_STEP,
        stepIndex
    }
}

export function finishStep(){

    return (dispatch)=> {
        dispatch({type : ActionTypes.FINISH_STEP, payload: null});
        dispatch(push('/projects/branches/files/translate'));
    }
}

