/**
 * Created by IMAD on 26/04/2017.
 */

import ActionTypes from '../constants/ActionTypes';



export function closeInfo() {
    return {
        type: ActionTypes.CLOSE_INFO,
        payload: false,
    }
}