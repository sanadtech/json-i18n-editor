/**
 * Created by IMAD on 13/04/2017.
 */
import ActionTypes from "../constants/ActionTypes";
import axios from "axios";
import {push} from "react-router-redux";
import filesaver from "file-saver";
import {generateJSON, recursiveObjectSource, recursiveObjectTarget} from "../utils/parseJSON";
import {commitFile, getFileContents} from "../utils/GitLabAPI";


function fetchSources (dispatch,res){
    let arr=recursiveObjectSource(res,'');
    dispatch({type: ActionTypes.FETCH_KEYWORDS_SOURCE_FULFILLED, keywords: arr,fetching:false , initialKeywords : res });
    dispatch({type: ActionTypes.NBR_EMPTY_KEYWORDS,payload:null});
}

function fetchTargets (dispatch,res){
    let  result= recursiveObjectTarget(res, '');
    dispatch({type: ActionTypes.FETCH_KEYWORDS_TARGET_FULFILLED, keywords: result ,fetching:false , initialKeywords : res });
    dispatch({type: ActionTypes.NBR_UNTRANSLATABLE_KEYWORDS,payload:null});
}

function error (dispatch,error) {
    dispatch({type: ActionTypes.FETCH_PROJECTS_REJECTED, error, fetching:false });
}

function getFileTarget (dispatch,getState,fileContent){

    fetchSources(dispatch,fileContent);
    dispatch({type: ActionTypes.FETCH_KEYWORDS_PENDING, fetching:true});
    getFileContents(
        getState().files.projectId,
        getState().files.targetFile.path,
        getState().login.privateToken,
        getState().files.branch,
        (fileContent)=> fetchTargets(dispatch,fileContent),
        (err) => error(dispatch,err)
    );
}

export function onResetFiles(){
    return(dispatch)=>{
        dispatch({type : ActionTypes.ADD_SOURCE_FILE, payload :  {id:'',path:'',name:''} });
        dispatch({type : ActionTypes.ADD_TARGET_FILE, payload :  {id:'',path:'',name:''} });
        dispatch({type: ActionTypes.FETCH_KEYWORDS_SOURCE_FULFILLED, keywords: [],fetching:false });
    }
}

export function addSourceFile(id,path,name){
    return (dispatch,getState)=> {
        dispatch({type : ActionTypes.ADD_SOURCE_FILE, payload :  {id,path,name} });
        dispatch({type: ActionTypes.FETCH_KEYWORDS_PENDING,fetching:true });
        getFileContents(
            getState().files.projectId,
            getState().files.sourceFile.path,
            getState().login.privateToken,
            getState().files.branch,
            fileContent=> fetchSources(dispatch,fileContent),
            (err) =>{
                dispatch({type : ActionTypes.ADD_SOURCE_FILE, payload : {id:'',path:'',name:''}  });
                error(dispatch,err)
            }
        );
    }
}

export function addTargetFile(id,path,name){
    return (dispatch,getState)=> {
        if (getState().files.sourceFile.path === '')
            error(dispatch, {status: 112, message: "you have not selected a reference file"});
        else {
            dispatch({type: ActionTypes.ADD_TARGET_FILE, payload: {id, path, name}});
            dispatch({type: ActionTypes.FINISH_STEP, payload: null});
            dispatch({type: ActionTypes.FETCH_KEYWORDS_PENDING, fetching: true});
            getFileContents(
                getState().files.projectId,
                getState().files.targetFile.path,
                getState().login.privateToken,
                getState().files.branch,
                (fileContent) => fetchTargets(dispatch, fileContent),
                (err) =>{
                    dispatch({type : ActionTypes.ADD_TARGET_FILE, payload : {id:'',path:'',name:''}  });
                    error(dispatch,err)
                }
            );
        }
    }
}

export function addFiles(files){
    return (dispatch,getState)=> {
        dispatch({type : ActionTypes.CHANGE_MODE, mode:'translate'});
        dispatch({type : ActionTypes.ADD_FILES, files});
        dispatch({type: ActionTypes.FETCH_KEYWORDS_PENDING,fetching:true });
        getFileContents(
            getState().files.projectId,
            getState().files.sourceFile.path,
            getState().login.privateToken,
            getState().files.branch,
            fileContent=> getFileTarget(dispatch,getState,fileContent),
            err => error(dispatch,err)
        );
        dispatch(push('/projects/branches/files/translate'));
    }
}

export function downloadFileTarget(){
    return  (dispatch,getState) => {
        dispatch({type: ActionTypes.FETCH_KEYWORDS_PENDING, fetching:true});
        let json = generateJSON(getState().keywords.keywords);
        let file = new window.File([JSON.stringify(json,null,4)], "target_file.json", {type: "text/json;charset=utf-8"});
        filesaver.saveAs(file);
        dispatch({type: ActionTypes.FETCH_KEYWORDS_PENDING, fetching:false});
    }
}

export function commitFileToGitlab(){
    return (dispatch,getState)=>{
        let file= ( getState().mode.mode==='translate') ? getState().files.targetFile.path :getState().files.sourceFile.path;
        dispatch({type: ActionTypes.FETCH_KEYWORDS_PENDING, fetching:true});
        let json = JSON.stringify( generateJSON( getState().keywords.keywords),null,2);
        commitFile(
            'PUT',
            getState().login.privateToken,
            getState().files.projectId,
            file,
            getState().files.branch,
            json,
            getState().form.commitForm.values.message,
            () => {
                dispatch({type: ActionTypes.COMMIT_KEYWORDS,committed:true,modified:false, fetching:false});
                dispatch( {type: ActionTypes.CLOSE_COMMIT_MODAL,open: false,info:2});
            },
            (err) => error(dispatch,err)
        )
    }
}

export function createFile() {
    return {type : ActionTypes.ADD_SOURCE_FILE, payload :  {id:'1',path:'path',name:'path'} };
}

export function getFilesFromGit(){
    return (dispatch,getState) =>{
        dispatch({type: ActionTypes.FETCH_KEYWORDS_PENDING,fetching:true });
        getFileContents(
            getState().files.projectId,
            getState().files.sourceFile.path,
            getState().login.privateToken,
            getState().files.branch,
            fileContent=> {
                if(getState().mode.mode==='translate') getFileTarget(dispatch,getState,fileContent)
                else  fetchSources(dispatch,fileContent)
            }
                ,
            err => error(dispatch,err)
        );
    }
}

export function getFileSourceFromGit(){
    return (dispatch,getState) =>{
        window.localStorage.setItem("historic", JSON.stringify(getState().historic.historic));
        dispatch({type: ActionTypes.FETCH_KEYWORDS_PENDING,fetching:true });
        getFileContents(
            getState().files.projectId,
            getState().files.sourceFile.path,
            getState().login.privateToken,
            getState().files.branch,
            fileContent=> fetchSources(dispatch,fileContent),
            (err) => error(dispatch,err)
        );
    }
}

export function getFileTargetFromGit(){
    return (dispatch,getState)=>{
        dispatch({type: ActionTypes.FETCH_KEYWORDS_PENDING, fetching:true});
        getFileContents(
            getState().files.projectId,
            getState().files.targetFile.path,
            getState().login.privateToken,
            getState().files.branch,
            (fileContent)=> fetchTargets(dispatch,fileContent),
            (err) => error(dispatch,err)
        );
    }
}

export function uploadfileSource(fileaccept) {
    return (dispatch)=> {
        dispatch({type: ActionTypes.FETCH_KEYWORDS_PENDING, payload: null});
        axios.get(fileaccept[0].preview)
            .then( (res)=> fetchSources(dispatch,res.data))
            .catch( (err) => error(dispatch,err.data.message));
    }
}

export function uploadfileTarget(fileaccept) {
    return (dispatch)=> {
        dispatch({type: ActionTypes.FETCH_KEYWORDS_PENDING, payload: null});
        axios.get(fileaccept[0].preview)
            .then( (res)=> fetchTargets(dispatch,res.data))
            .catch((err) =>error(dispatch,err.data.message));
    }
}