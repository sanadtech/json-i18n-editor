/**
 * Created by IMAD on 18/04/2017.
 */

import {GetTree} from '../utils/GitLabAPI';
import ActionTypes from '../constants/ActionTypes';



export  function fetchTree()
{
    return (dispatch,getState)=>{
        dispatch({type: ActionTypes.FETCH_TREE_PENDING,fetching: true});
        GetTree(
            getState().login.privateToken,
            getState().files.projectId,
            getState().files.branch,
            '',
            result=>dispatch({type: ActionTypes.FETCH_TREE_FULFILLED, list: result ,pathname:'',fetching:false}),
            error=>dispatch({type: ActionTypes.FETCH_TREE_REJECTED, error, fetching:false})
        );

    }
}

export  function fetchChildren(pathname)
{
    return (dispatch,getState)=>{
        dispatch({type: ActionTypes.FETCH_TREE_PENDING,fetching: true})
        GetTree(
            getState().login.privateToken,
            getState().files.projectId,
            getState().files.branch,
            pathname,
            result=> dispatch({type: ActionTypes.FETCH_TREE_CHILDREN_FULFILLED, list: result ,pathname,fetching:false}),
            error=>  dispatch({type: ActionTypes.FETCH_TREE_REJECTED, error, fetching:false})
        );
    }
}

