/**
 * Created by IMAD on 25/05/2017.
 */

import {push, replace} from "react-router-redux";
import ActionTypes from "../constants/ActionTypes";


function redirect(dispatch,newPage){
    switch (newPage){
        case 'projects' :
            dispatch({type: ActionTypes.FETCH_KEYWORDS_SOURCE_FULFILLED, keywords: [],fetching:false });
            dispatch({type: ActionTypes.FETCH_BRANCHES_FULFILLED, branches: [] ,fetching:false});
            dispatch({type: ActionTypes.FETCH_TREE_FULFILLED, list: [], pathname: '', fetching: false});
            dispatch(push('/'));
            break;

        case 'branches' :
            dispatch({type: ActionTypes.FETCH_BRANCHES_FULFILLED, branches: [] ,fetching:false});
            dispatch({type: ActionTypes.FETCH_TREE_FULFILLED, list: [], pathname: '', fetching: false});
            dispatch(push('/projects/branches'));
            break;

        case 'files' :
            dispatch({type: ActionTypes.FETCH_TREE_FULFILLED, list: [] ,pathname:'',fetching:false});
            dispatch(push('/projects/branches/files'));
            break;

        case 'signout' :
            window.localStorage.removeItem('privateToken');
            window.localStorage.removeItem('username');
            window.localStorage.removeItem('expirateAt');
            dispatch( {type:ActionTypes.INITIAL_STATE,payload:null});
            dispatch(replace('/'));
            //logout();
            break;

        default:
            dispatch(push('/'));
            break;
    }
}


export function ChangeRoute(newPage){
    return (dispatch,getState)=>{
        dispatch({ type: ActionTypes.CLOSE_FORM, open: false });

        const state = getState();

        if (
            state.routing.locationBeforeTransitions &&
            state.routing.locationBeforeTransitions.pathname === '/projects/branches/files/translate' &&
            getState().keywords.modified
        )
            dispatch({type: ActionTypes.OPEN_NAVIGATION_MODAL, open:true, newPage});
        else{
            redirect(dispatch,newPage)
        }
    }
}

export function RedirectToNewPage(){
    return (dispatch,getState)=>{
        redirect(dispatch, getState().modal.navigationModal.newPage)
    }
}