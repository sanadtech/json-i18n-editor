/**
 * Created by IMAD on 07/05/2017.
 */

import {black, green500, grey50, red500, white, yellow500, grey100, grey300, grey400, grey500, darkBlack, fullBlack,} from "material-ui/styles/colors";
import {fade} from 'material-ui/utils/colorManipulator';
import Spacing from 'material-ui/styles/spacing';
import zIndex from 'material-ui/styles/zIndex';


export default {

    columns: {
        margin: 'inherit',
        padding: 'inherit',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        alignContent: 'center'
    },

    progress : {
        height: '5px',
        backgroundColor : black
    },

    title : {
        height: '70px',
        marginBottom : '20px',
        backgroundColor : black
    },

    preview:{
        borderTop: '1px solid #e5e5e5',
        borderRadius: '1px',
        overflow : 'auto',
        height:'200px',
    },

    yellowText :{
        color :  grey500,
    },

    black :{
        backgroundColor :  "#D98F00",
        color :  "#D98F00",
    },

    oddLine :{
        backgroundColor : grey50
    },

    pairLine :{
        backgroundColor :  fade(darkBlack, 0.1)
    },

    tabBorder:{
        borderTop: '1px solid #e5e5e5',
        borderRadius: '1px',
    },

    theme :{
        spacing: Spacing,
        zIndex: zIndex,
        tabs: {
            textColor: black,
            selectedTextColor: "#D98F00",
            backgroundColor: white,
            width: '50%'
        },
        palette: {
            primary1Color: "#F0C105",
            primary2Color: "#D98F00",
            primary3Color: grey400,
            accent1Color: white,
            accent2Color: grey100,
            accent3Color: grey500,
            textColor: darkBlack,
            alternateTextColor: white,
            canvasColor: white,
            borderColor: grey300,
            disabledColor: fade(darkBlack, 0.3),
            pickerHeaderColor: "#D98F00",
            clockCircleColor: fade(darkBlack, 0.07),
            shadowColor: fullBlack,
        },

    },

    modal:{
        height:'660px',
        width: '70%',
        maxWidth: 'none'
    },

    tabItemContainerStyle :{
        width: '40%'
    },

    titleColumn: {
        display: 'flex',
        flex: '1 1 75px',
        flexDirection: 'column',
        width: '100%',
        height: '80px',
        position: 'fixed',
        zIndex:99,
        backgroundColor: white,
    },

    fullWidth:{
        width: '100%',
    },

    fixed:{
        position: 'fixed',
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        alignContent: 'center',
        width: '100%',
        height:'75px',
        zIndex:100,
        backgroundColor: white,
    },

    fixedContainer:{
        position: 'fixed',
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        alignContent: 'center',
        width: '100%',
        height:'125px',
        zIndex:100,
        backgroundColor: white,
    },

    afterFixed:{
        marginTop:'75px',
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        alignContent: 'center',
        width: '100%',
        height:'100%',
    },

    afterFixedContainer:{
        marginTop:'125px',
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        alignContent: 'center',
        width: '100%',
        height:'100%',
    },

    mainColumn: {
        display: 'flex',
        marginTop: '75px',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor : white,
        alignContent: 'center',
        width: '100%',
    },

    list :{
        width: '100%' ,
        height:'100%',
    },

    form :{
        width: '90%'
    },

    Progress :{
        width: '90%',
        margin: 30,
        padding : 5
    },
    formLogin :{
        width: '50%'
    },

    buttons: {
        marginTop: '24px',
        marginRight: '15px',
    },

    container: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        alignContent: 'center',
        backgroundColor: white,
        margin: '0px 5px 5px 5px',
        width: '100%',
        height:'100%',
    },

    header: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        padding : '0px 5px 5px 5px',
        margin: '0px 5px 5px 5px',
        width: '90%',
        height:'100%',
        backgroundColor: white,
    },
    row: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        padding : '5px',
        margin: '5px',
        width: '90%',
        height:'100%',
        backgroundColor: white,
    },

    DiffComponent: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        padding : '15px',
        margin: '15px',
        width: '100%',
        backgroundColor: white,
    },

    field:{
        width : '90%'
    },

    tableColumn:{
        margin:'0 10px 0 5px'
    },

    eltRow:{
        flex : 1,
        width:'50%',
        marginLeft:'15px',
        backgroundColor: white,
    },
    elt:{
        flex : 1,
        width:'30%',
        marginLeft:'20px',
        backgroundColor: white,
    },

    largeIcon: {
        width: 60,
        height: 60,
    },

    large: {
        width: 120,
        height: 120,
        padding: 30,
    },

    disabled:{
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        alignContent: 'center',
        width: '100%',
        height:'100%',
        opacity : 0.5 ,
        backgroundColor: grey50
    },

    table:{
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        alignContent: 'center',
        width: '100%',
        height:'100%',
        minHeight:'100px',
    },

    highlight :{
        backgroundColor: yellow500
    },

    chat:{
        position: 'fixed',
        display: 'flex',
        left:'90%',
        bottom: 20,
        zIndex: 100
    },

    popover:{
        display: 'flex',
        flexDirection: 'column-reverse',
        width: '400px',
        height:'400px',
        margin: 0 ,
        padding: 0
    },

    popList:{
        display : 'flex',
        flexDirection: 'column-reverse',
        flex: 1,
        height:'290px',
        overflow: 'auto',
        padding: 0
    },

    popText:{
        display: 'flex',
        flex:'1 1 80px',
        margin: 0 ,
        padding: 0,
        zIndex: 20,
    },

    markdown : {
        width: '100%',
        height: '100%',
        overflow: 'auto',
        margin: '10px auto',
        border: '1px solid #e5e5e5',
        borderRadius: '1px',
        transition: 'border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s'
    },

    headline: {
        fontSize: 24,
        paddingTop: 16,
        margin: 12,
    },

    toggle:{
        marginTop:'8px',
    },

    successfulMsg:{
        margin : "0px 15px 0 15px",
        color: green500,
        zIndex: 4000,
    },

    failedMsg:{
        margin : "0px 15px 0 15px",
        color: red500,
        zIndex: 4000,
    },

    modifiedMsg:{
        margin : "0px 15px 0 15px",
        color :  "#D98F00",
    }
}





